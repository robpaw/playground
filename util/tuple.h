#pragma once

#include "mpl.h"

#include <tuple>

template<typename Stream, typename ... Ts, size_t I, size_t ... Is>
void toStream(Stream & s, const std::tuple<Ts...> & t, Indices<I, Is...>, const std::string & delim) {
  s << std::to_string(std::get<I>(t));
  Expand { (s << delim << std::to_string(std::get<Is>(t)))... };
}

template<typename Stream, typename ... Ts>
void toStream(Stream & s, const std::tuple<Ts...> & t, const std::string & delim) {
  toStream(s, t, IndicesFor<std::tuple<Ts...>>{}, delim);
}

template<typename ... Ts>
std::ostream & operator<< (std::ostream & s, const std::tuple<const Ts &...> & t) {
  s << "{";
  toStream(s, t, ", ");
  s << "}";
  return s;
}

