#pragma once

#include <algorithm>

namespace stl {

template <class ForwardIt>
ForwardIt rotate(ForwardIt first, ForwardIt n_first, ForwardIt last) {
  if(first == n_first) return last;
  if(n_first == last) return first;
 
  ForwardIt next = n_first;
 
  do {
    using std::iter_swap;
    iter_swap(first++, next++);
    if (first == n_first) n_first = next;
  }
  while (next != last);
 
  ForwardIt ret = first;
 
  for(next = n_first; next != last; ) {
    using std::iter_swap;
    iter_swap(first++, next++);
    if(first == n_first) n_first = next;
    else if(next == last) next = n_first;
  }
 
  return ret;
}

template<class ForwardIt, class UnaryPredicate>
ForwardIt partition(ForwardIt first, ForwardIt last, UnaryPredicate p) {
  first = std::find_if_not(first, last, p);
  if (first == last) return first;
 
  for(ForwardIt i = std::next(first); i != last; ++i) {
    if(p(*i)){
      using std::iter_swap;
      iter_swap(i, first);
      ++first;
    }
  }
  return first;
}

template <typename ForwardIt, typename Compare>
void sort(ForwardIt first, ForwardIt last, Compare compare = std::less<typename ForwardIt::value_type>{}) {
  auto dist = std::distance(first,last);
  if(dist == 0) return;
  if(dist <= 32) {
    // insertion sort
    for (auto i = first; i != last; ++i) {
      stl::rotate(std::upper_bound(first, i, *i, compare), i, std::next(i));
    }
  } else {
    // quicksort
    auto pivot = *std::next(first, dist/2);
    ForwardIt middle1 = stl::partition(first, last, [pivot, compare](const auto& em){ return compare(em, pivot); });
    ForwardIt middle2 = stl::partition(middle1, last, [pivot, compare](const auto& em){ return !(compare(pivot, em)); });
    stl::sort(first, middle1, compare);
    stl::sort(middle2, last, compare);
  }
}

}

