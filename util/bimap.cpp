
#include <vector>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <algorithm>

#include <cassert>

template<typename T1, typename T2, typename C1 = std::less<T1>, typename C2 = std::less<T2> >
class Bimap {
    typedef std::pair<T1, T2> Pair;
    typedef std::vector<Pair> Storage;
    typedef typename Storage::iterator iterator;
    typedef typename Storage::const_iterator const_iterator;
    typedef typename Storage::reverse_iterator reverse_iterator;
    typedef typename Storage::const_reverse_iterator const_reverse_iterator;

 private:
    typedef std::vector<size_t> IndexVector;

    template<typename T, typename It = typename IndexVector::iterator, typename Base = std::iterator<std::random_access_iterator_tag, T> >
    struct Iterator : Base {
        Iterator(It it, Bimap* bimap) : it(it), bimap(bimap) {}

        using typename Base::reference;
        using typename Base::pointer;
        using typename Base::difference_type;

        reference operator[](size_t i) { return bimap->storage[it[i]]; }
        reference operator*() { return bimap->storage[*it]; }
        pointer operator->() { return &bimap->storage[*it]; }

        Iterator& operator++() { ++it; return *this; }
        Iterator operator++(int) { Iterator tmp(it); ++it; return tmp; }
        Iterator& operator--() { --it; return *this; }
        Iterator operator--(int) { Iterator tmp(it); --it; return tmp; }

        Iterator& operator+=(size_t n) { it += n; return *this; }
        Iterator& operator-=(size_t n) { it -= n; return *this; }
        Iterator operator-(size_t n) { Iterator tmp(it - n); return tmp; }
        friend Iterator operator+(size_t n, const Iterator& it) { return Iterator(it.it + n); }
        friend Iterator operator+(const Iterator& it, size_t n) { return Iterator(it.it + n); }

        difference_type operator-(const Iterator& rhs) { return it - rhs.it; }

        bool operator==(const Iterator& rhs) const { return it == rhs.it; }
        bool operator!=(const Iterator& rhs) const { return !operator==(rhs); }
        bool operator<(const Iterator& rhs) const { return it < rhs.it; }
        bool operator<=(const Iterator& rhs) const { return it <= rhs.it; }
        bool operator>(const Iterator& rhs) const { return it > rhs.it; }
        bool operator>=(const Iterator& rhs) const { return it >= rhs.it; }
    private:
        It it;
        Bimap* bimap;
    };

    struct CompareWithFirst {
        CompareWithFirst(const Storage& storage) : storage(storage) {}
        bool operator()(const T1* lhs, size_t rhs) const {  return comp(*lhs, storage[rhs].first); }
        bool operator()(size_t lhs, const T1* rhs) const {  return comp(storage[lhs].first, *rhs); }
        const Storage& storage;
        C1 comp;
    };

    struct CompareWithSecond {
        CompareWithSecond(const Storage& storage) : storage(storage) {}
        bool operator()(const T2* lhs, size_t rhs) const { return comp(*lhs, storage[rhs].second); }
        bool operator()(size_t lhs, const T2* rhs) const { return comp(storage[lhs].second, *rhs); }
        const Storage& storage;
        C2 comp;
    };

public:
    template<typename Key, typename Val, typename Base, typename Compare>
    class Map : protected Base {
        using Base::bimap;

    public:
        typedef Bimap::Iterator<Pair> iterator;
        typedef Bimap::Iterator<const Pair> const_iterator;
        typedef std::reverse_iterator<Bimap::Iterator<Pair> > reverse_iterator;
        typedef std::reverse_iterator<Bimap::Iterator<const Pair> > const_reverse_iterator;

    public:
        Map(Bimap& bimap, IndexVector& map) : Base(bimap), map(&map) {}

        void insert(const Key& key, const Val& value) { Base::insert(key, value); }
        void erase(const Key& key) { iterator it = find(key); if(it != end()) Base::erase(*it); }

        const Val& operator[](const Key& key) const {
            return at(key);
        }
        const Val& at(const Key& key) const {
            const_iterator it = find(key);
            if(it == end()) std::runtime_error("element not found");
            return Base::value(*it);
        }

        bool contains(const Key& key) const { return find(key) != end(); }

        iterator find(const Key& key) { return iterator(std::lower_bound(map->begin(), map->end(), &key, Compare(bimap->storage)), bimap); }
        const_iterator find(const Key& key) const { return iterator(std::lower_bound(map->begin(), map->end(), &key, Compare(bimap->storage)), bimap); }

        iterator begin() { return iterator(map->begin(), bimap); }
        const_iterator begin() const { return const_iterator(map->begin(), bimap); }
        const_iterator cbegin() const { return const_iterator(map->begin(), bimap); }

        iterator end() { return iterator(map->end(), bimap); }
        const_iterator end() const { return const_iterator(map->end(), bimap); }
        const_iterator cend() const { return const_iterator(map->end(), bimap); }

        reverse_iterator rbegin() { return reverse_iterator(map->begin(), bimap); }
        const_reverse_iterator rbegin() const { return const_reverse_iterator(map->begin(), bimap); }
        const_reverse_iterator crbegin() const { return const_reverse_iterator(map->begin(), bimap); }

        reverse_iterator rend() { return reverse_iterator(map->end(), bimap); }
        const_reverse_iterator rend() const { return const_reverse_iterator(map->end(), bimap); }
        const_reverse_iterator crend() const { return const_reverse_iterator(map->end(), bimap); }

        size_t size() const { return map->size(); }
        bool empty() const { return map->empty(); }

    private:
        IndexVector* map;
    };

    struct Base12 {
        Base12(Bimap& bimap) : bimap(&bimap) {}
        void insert(const T1& k, const T2& v) { bimap->insert(Pair(k, v)); }
        void erase(const Pair& p) { bimap->erase(p); }
        T1& key(Pair& p) const { return p.first; }
        const T1& key(const Pair& p) const { return p.first; }
        T2& value(Pair& p) const { return p.second; }
        const T2& value(const Pair& p) const { return p.second; }
        Bimap* bimap;
    };
    struct Base21 {
        Base21(Bimap& bimap) : bimap(&bimap) {}
        void insert(const T2& k, const T1& v) { bimap->insert(Pair(v, k)); }
        void erase(const Pair& p) { bimap->erase(p); }
        T2& key(Pair& p) const { return p.second; }
        const T2& key(const Pair& p) const { return p.second; }
        T1& value(Pair& p) const { return p.first; }
        const T1& value(const Pair& p) const { return p.first; }
        Bimap* bimap;
    };

    typedef Map<T1, T2, Base12, CompareWithFirst> First;
    typedef Map<T2, T1, Base21, CompareWithSecond> Second;

public:
    First first() { return First(*this, map12); }
    const First first() const { return First(*this, map12); }

    Second second() { return Second(*this, map21); }
    const Second second() const { return Second(*this, map21); }

    iterator begin() { return iterator(storage.begin()); }
    const_iterator begin() const { return const_iterator(storage.begin()); }
    const_iterator cbegin() const { return const_iterator(storage.begin()); }

    iterator end() { return storage.end(); }
    const_iterator end() const { return storage.end(); }
    const_iterator cend() const { return storage.cend(); }

    reverse_iterator rbegin() { return reverse_iterator(storage.begin()); }
    const_reverse_iterator rbegin() const { return storage.rbegin(); }
    const_reverse_iterator crbegin() const { return storage.crbegin(); }

    reverse_iterator rend() { return reverse_iterator(storage.end()); }
    const_reverse_iterator rend() const { return storage.rend(); }
    const_reverse_iterator crend() const { return storage.crend(); }

    size_t size() const { return storage.size(); }
    bool empty() const { return storage.empty(); }

    void insert(const Pair& p) {
        map12.reserve(map12.size() + 1);
        map21.reserve(map21.size() + 1);

        typedef IndexVector::iterator It;
        typedef std::pair<It, It> ItPair;

        ItPair it1 = std::equal_range(map12.begin(), map12.end(), &p.first, CompareWithFirst(storage));
        if(it1.first != it1.second) throw std::runtime_error("first element already exists");

        ItPair it2 = std::equal_range(map21.begin(), map21.end(), &p.second, CompareWithSecond(storage));
        if(it2.first != it2.second) throw std::runtime_error("second element already exists");

        storage.push_back(p);

        map12.insert(it1.second, storage.size() - 1);
        map21.insert(it2.second, storage.size() - 1);
    }
    void erase(const Pair& p) {
        typedef IndexVector::iterator It;
        typedef std::pair<It, It> ItPair;

        ItPair it1 = std::equal_range(map12.begin(), map12.end(), &p.first, CompareWithFirst(storage));
        if(it1.first == it1.second) return;

        ItPair it2 = std::equal_range(map21.begin(), map21.end(), &p.second, CompareWithSecond(storage));
        if(it2.first == it2.second) return;

        assert(*it1.first == *it2.first);

        Pair& erased = storage[*it1.first];
        Pair& last = storage.back();
        ItPair it1Last = std::equal_range(map12.begin(), map12.end(), &last.first, CompareWithFirst(storage));
        ItPair it2Last = std::equal_range(map21.begin(), map21.end(), &last.second, CompareWithSecond(storage));

        assert(it1Last.first != it1Last.second);
        assert(it2Last.first != it2Last.second);

        std::swap(erased, last);
        std::swap(*it1Last.first, *it1.first);
        std::swap(*it2Last.first, *it2.first);

        storage.pop_back();

        map12.erase(it1.first, it1.second);
        map21.erase(it2.first, it2.second);
    }

private:
    Storage storage;
    IndexVector map12;
    IndexVector map21;
};

template<typename Map>
void print(Map& map) {
    {
        typedef typename Map::First::iterator It;
        std::cout << "iterating..." << std::endl;
        It it = map.first().begin();
        for(; it != map.first().end(); ++it) {
            std::cout << it->first << " -> " << it->second << std::endl;
        }
    }
    {
        typedef typename Map::Second::iterator It;
        It it = map.second().begin();
        for(; it != map.second().end(); ++it) {
            std::cout << it->first << " <- " << it->second << std::endl;
        }
    }
}

int main(int argc, char** argv) {
    typedef Bimap<int, std::string> MyBimap;

    MyBimap bimap;

    std::cout << "inserting..." << std::endl;
    bimap.first().insert(1, "f");
    bimap.first().insert(2, "e");
    bimap.first().insert(3, "d");

    bimap.second().insert("c", 4);
    bimap.second().insert("b", 5);
    bimap.second().insert("a", 6);

    print(bimap);

    try {
        bimap.first().insert(10, "a");
        std::cout << "it should not have happened..." << std::endl;
    } catch(...) {}

    try {
        bimap.second().insert("a", 10);
        std::cout << "it should not have happened..." << std::endl;
    } catch(...) {}

    print(bimap);

    std::cout << "erasing..." << std::endl;
    bimap.first().erase(4);
    print(bimap);
    bimap.first().erase(6);
    print(bimap);
    bimap.first().erase(5);
    print(bimap);

    bimap.second().erase("f");
    print(bimap);
    bimap.second().erase("d");
    print(bimap);
    bimap.second().erase("e");
    print(bimap);

    return 0;
}
