
#include <type_traits>
#include <string>
#include <iostream>
#include <bitset>
#include <sstream>

template<size_t ... Is>
struct Indices {};

template<size_t ... Is>
struct IndexBuilder {};
template<size_t I, size_t ... Is>
struct IndexBuilder<I, Is ...> {
    using type = typename IndexBuilder<I - 1, I - 1, Is ...>::type;
};
template<size_t ... Is>
struct IndexBuilder<0, Is ...> {
    using type = Indices<Is...>;
};

template<size_t N, size_t ... Is>
struct ReverseIndexBuilder {
    using type = typename ReverseIndexBuilder<N, 0, Is ...>::type;
};
template<size_t N, size_t I, size_t ... Is>
struct ReverseIndexBuilder<N, I, Is ...> {
    using type = typename ReverseIndexBuilder<N, I + 1, Is ...>::type;
};
template<size_t N, size_t ... Is>
struct ReverseIndexBuilder<N, N, Is ...> {
    using type = Indices<Is...>;
};

template<size_t N>
using BuildIndices = typename IndexBuilder<N>::type;

template<size_t N>
using BuildIndicesReversed = typename ReverseIndexBuilder<N>::type;

// ===========================

template<size_t>
struct IndexToType;

template<typename T>
struct TypeToIndex;

#define DEF(INDEX, TYPE) \
    template<> struct IndexToType<INDEX> { using type = TYPE; }; \
    template<> struct TypeToIndex<TYPE> { constexpr static size_t value = INDEX; };

DEF(0, char)
DEF(1, bool)
DEF(2, int8_t)
DEF(3, uint8_t)
DEF(4, int16_t)
DEF(5, uint16_t)
DEF(6, int32_t)
DEF(7, uint32_t)
DEF(8, int64_t)
DEF(9, uint64_t)
DEF(10, float)
DEF(11, double)

template<typename T, size_t V = 0, size_t B = sizeof(size_t) * 8>
struct EncodeType {
    static_assert(B > 3, "too many levels of cv");
    static constexpr size_t value = V | TypeToIndex<T>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<const T, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x2UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<volatile T, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x4UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<const volatile T, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x6UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<T*, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x1UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<const T*, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x3UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<volatile T*, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x5UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};
template<typename T, size_t V, size_t B>
struct EncodeType<const volatile T*, V, B> {
    static_assert(B > 6, "too many levels of cv");
    static constexpr size_t value = EncodeType<T, (0x7UL << (sizeof(size_t)*8 - 3)) | (V >> 3), B - 3>::value;
};

template<size_t V, size_t N>
struct Msb {
    constexpr static size_t value = (V >> (sizeof(V) * 8 - N));
};

template<typename T, size_t V, size_t I = Msb<V, 3>::value > struct DecodeCV {};
template<typename T> struct DecodeCV<T, 0, 0x0> { using type = T; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x1> { using type = typename DecodeCV<T, V << 3>::type*; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x2> { using type = typename DecodeCV<const T, V << 3>::type; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x3> { using type = typename DecodeCV<const T*, V << 3>::type; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x4> { using type = typename DecodeCV<volatile T, V << 3>::type; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x5> { using type = typename DecodeCV<volatile T*, V << 3>::type; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x6> { using type = typename DecodeCV<const volatile T, V << 3>::type; };
template<typename T, size_t V> struct DecodeCV<T, V, 0x7> { using type = typename DecodeCV<const volatile T*, V << 3>::type; };

template<size_t V, typename T = typename IndexToType<V & 0xF>::type >
struct DecodeType {
    using type = typename DecodeCV<T, V & (~static_cast<size_t>(0xF))>::type;
};

// ===========================

template<typename T>
struct Indentity {
    using type = T;
};

template<typename T, size_t I>
struct TypeFlag {
    constexpr TypeFlag() { }
    friend auto flag(TypeFlag<T, I>);
    using type = T;
    constexpr static size_t index = I;
};

template<typename F, typename U>
struct TypeFlagImpl {
    constexpr TypeFlagImpl() { }
    friend auto flag(TypeFlag<typename F::type, F::index>) { return U{}; }
};

template<typename F, typename U, size_t = sizeof(TypeFlagImpl<F, U>)>
struct TypeFlagDef { };

template<typename T, typename ... Args>
auto isBracesConstructibleTest(long) -> decltype((T{ Args{}... }, std::true_type{}));

template<typename T, typename ... Args>
auto isBracesConstructibleTest(int) -> std::false_type;

template<typename T, typename ... Args>
using isBracesConstructible = decltype(isBracesConstructibleTest<T, Args...>(0L));

template<typename U>
struct Value {
    constexpr static U value = {};
};

template<typename T>
struct CastToTuple;

template<typename T, size_t I>
struct Member {
    template<typename U, typename = typename std::enable_if< !std::is_class<U>::value >::type, size_t = sizeof(TypeFlagImpl<TypeFlag<T, I>, U>)>
    constexpr operator const U () const { return U{}; }

    template<typename U, typename = typename std::enable_if< std::is_class<U>::value >::type, size_t = sizeof(TypeFlagImpl<TypeFlag<T,I>, typename CastToTuple<U>::type>)>
    constexpr operator U () const { return U{}; }

//    template<typename U>
//    constexpr operator typename std::enable_if<std::is_class<U>::value, const U&>::type() const { return TypeFlagDef<TypeFlag<T, I>, typename CastToTuple<U>::type>{}; }
};

template<typename T, size_t ... Is>
auto isNArgsConstructibleTest(Indices<Is...>) -> isBracesConstructible<T, Member<T, Is> ...>;

template<typename T, size_t N>
using isNArgsConstructible = decltype(isNArgsConstructibleTest<T>(BuildIndices<N>{}));

template<typename T>
struct IsSupported {
    constexpr static bool value = std::is_pod<T>::value && std::is_class<T>::value;
};
template<typename T>
struct IsSupportedAssert : IsSupported<T> {
    static_assert(IsSupported<T>::value, "only POD structures are supported");
};

template<typename T, size_t N, bool = isNArgsConstructible<T, N>::value>
struct MembersCounter {
    using type = typename MembersCounter<T, N - 1>::type;
};

template<typename T, size_t N>
struct MembersCounter<T, N, true> {
    using type = std::integral_constant<size_t, N>;
};

template<typename T, bool = IsSupportedAssert<T>::value>
using MembersCount = typename MembersCounter<T, sizeof(T)>::type;

template<typename T, size_t I, size_t = MembersCount<T>::value>
struct TypeOfField {
    using type = decltype(flag(TypeFlag<T, I>{}));
};

template<typename T>
struct TupleBase {
    constexpr TupleBase() {}
    constexpr TupleBase(T t) : t(std::forward<T>(t)) {}
    T t;
};

template<typename T, typename ... Ts>
struct Tuple : TupleBase<T>, Tuple<Ts...> {
    constexpr Tuple() {}
    constexpr Tuple(T t, Ts ... ts) : TupleBase<T>(std::forward<T>(t)), Tuple<Ts...>(std::forward<Ts>(ts)...) {}
};
template<typename T>
struct Tuple<T> : TupleBase<T> {
    constexpr Tuple() {}
    constexpr Tuple(T t) : TupleBase<T>(std::forward<T>(t)) {}
};

template<size_t I, typename T, typename ... Ts>
struct Get {
    static auto value(Tuple<T, Ts...>& t) -> decltype(Get<I - 1, Ts...>::value(t)) {
        return Get<I - 1, Ts...>::value(t);
    }
    static auto value(const Tuple<T, Ts...>& t) -> decltype(Get<I - 1, Ts...>::value(t)) {
        return Get<I - 1, Ts...>::value(t);
    }
};
template<typename T, typename ... Ts>
struct Get<0, T, Ts...> {
    static T& value(Tuple<T, Ts...>& t) {
        return static_cast<TupleBase<T>&>(t).t;
    }
    static const T& value(const Tuple<T, Ts...>& t) {
        return static_cast<const TupleBase<T>&>(t).t;
    }
};
template<size_t I, typename ... Ts>
auto get(Tuple<Ts...>& t) -> decltype(Get<I, Ts...>::value(t)) {
    return Get<I, Ts...>::value(t);
}
template<size_t I, typename ... Ts>
auto get(const Tuple<Ts...>& t) -> decltype(Get<I, Ts...>::value(t)) {
    return Get<I, Ts...>::value(t);
}

template<typename T, size_t ... Is>
auto TupleBuilder(Indices<Is...>) -> Tuple< typename TypeOfField<T, Is>::type...>;

template<typename T>
struct CastToTuple {
    using type = decltype(TupleBuilder<T>(BuildIndices<MembersCount<T>::value>{}));
};

template<typename T, typename U = typename CastToTuple<T>::type>
U& castToTuple(T& t) {
    return reinterpret_cast<U&>(t);
}

template<typename T, typename U = typename CastToTuple<T>::type>
const U& castToTuple(const T& t) {
    return reinterpret_cast<const U&>(t);
}

template<size_t I, typename S, typename = typename std::enable_if< IsSupported<S>::value >::type>
auto get(S& s) -> decltype(get<I>(castToTuple(s))) {
    return get<I>(castToTuple(s));
}

template<size_t I, typename S, typename = typename std::enable_if< IsSupported<S>::value >::type>
auto get(const S& s) -> decltype(get<I>(castToTuple(s))) {
    return get<I>(castToTuple(s));
}

template<typename S, typename = typename std::enable_if< IsSupported<S>::value >::type>
constexpr size_t size(const S& s) {
    return MembersCount<S>::value;
}

template<typename S, typename = typename std::enable_if< IsSupported<S>::value >::type>
constexpr size_t size() {
    return MembersCount<S>::value;
}

template<typename F, typename ... Ts, size_t ... Is>
void foreach(Tuple<Ts...>& t, F f, Indices<Is...>) {
      int arr[] = { ((void)f(get<Is>(t)), 0)... };
}

template<typename S, typename F, size_t N = size<S>()>
void foreach(S& s, F f) {
    auto& t = castToTuple(s);
    foreach(t, f, BuildIndices<N>{});
}

template<typename F, typename ... Ts, size_t ... Is>
void foreach(const Tuple<Ts...>& t, F f, Indices<Is...>) {
      int arr[] = { ((void)f(get<Is>(t)), 0)... };
}

template<typename S, typename F, size_t N = size<S>()>
void foreach(const S& s, F f) {
    const auto& t = castToTuple(s);
    foreach(t, f, BuildIndices<N>{});
}

template<typename S, typename = typename std::enable_if<std::is_class<S>::value && std::is_pod<S>::value>::type>
std::ostream& operator<< (std::ostream& o, const S& s) {
    o << '{';
    bool f = false;
    foreach(s, [&o, &f] (const auto& attr) mutable {
        if(f) {
            o << "; ";
        }
        o << attr;
        f = true;
    });
    o << '}';
    return o;
}

template<typename S, typename = typename std::enable_if<std::is_class<S>::value && std::is_pod<S>::value>::type>
std::istream& operator>> (std::istream& i, S& s) {
    char ch;
    i >> ch; // ’{’
    bool f = false;
    foreach(s, [&i, &f] (auto& attr) mutable {
        if(f) {
            char ch;
            i >> ch; // ';'
        }
        i >> attr;
        f = true;
    });
    i >> ch; // '}'
    return i;
}


struct A {
    int a;
};

struct B {
    int a;
    long b;
};

struct C {
    enum En {
        BLA
    } en;
    char c[2];
    struct {
        int d;
        long e;
    };
    union Un {
        long l;
        double d;
    } un;
};

int main(int argc, char ** argv) {

//    static_assert(std::is_same< std::tuple<int>, Tuple<A> >::value, "");
    //static_assert(!IsFlagSet<B, 0, 5>::value,  "");
//    static_assert(MembersCount<C>::value || false, "");

    C c;
    std::cout << size(c) << std::endl;
    std::cout << size<C>() << std::endl;
    static_assert(std::is_same< Tuple<C::En, char, char, Tuple<int, long>, C::Un>, typename CastToTuple<C>::type >::value, "");

    //sizeof(TypeFlagImpl<B, 1, 5>{});
    //static_assert(IsFlagSet<B, 0, 5>::value,  "");
    //static_assert(IsFlagSet<B, 1, 5>::value,  "");

    //static_assert(flag(TypeFlag<B, 1>{}) == 5, "");
    static_assert(MembersCount<B>::value == 2, "");
    static_assert(std::is_same< TypeOfField<B, 0>::type, int >::value, "");
    static_assert(std::is_same< TypeOfField<B, 1>::type, long >::value, "");
    //static_assert(std::is_same< TypeOfField<A, 0>, int >::value, "");

//    static_assert(std::is_same<int, typename TypeOfField<B, 0>::type>::value,  "");
//    static_assert(std::is_same<long, typename TypeOfField<B, 1>::type>::value,  "");
    static_assert(std::is_same<Tuple<int, long>, typename CastToTuple<B>::type >::value,  "");
    static_assert(std::is_same<Tuple<int>, typename CastToTuple<A>::type >::value,  "");

    B b = { 1, 2 };
    std::cout << b.a << std::endl;
    std::cout << b.b << std::endl;
    auto& t = castToTuple(b);
    get<0>(t) = 3;
    get<1>(t) = 4;
    std::cout << b.a << std::endl;
    std::cout << b.b << std::endl;

    get<0>(b) = 50;
    get<1>(b) = 60;
    std::cout << b.a << std::endl;
    std::cout << b.b << std::endl;

    using test = const float* const * volatile;
    std::cout <<  std::bitset<64>(EncodeType<test>::value) << std::endl;

    static_assert(std::is_same<test, typename DecodeType<EncodeType<test>::value>::type>::value, "");

    std::cout << b << std::endl;

    std::istringstream input;
    input.str("{ 100; 200 }");
    input >> b;
    std::cout << b << std::endl;


    return 0;
}
