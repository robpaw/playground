#pragma once

#include <type_traits>

#include <cstdint>

template<typename T>
constexpr T nextMultiple(T value, T multiple) {
  static_assert(std::is_integral<T>::value, "invalid type");
  return ((value + multiple - 1) / multiple) * multiple;
}

template<size_t M, typename T>
constexpr T nextMultiple(T value) {
  static_assert(std::is_integral<T>::value, "invalid type");
  return ((value + M - 1) / M) * M;
}

template<size_t N>
constexpr uint32_t nextPower(uint32_t v);

template<>
constexpr uint32_t nextPower<2>(uint32_t v) {
  assert(v > 0);
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  return v;
}
