#pragma once

#include "bitset.h"
#include "storage.h"
#include "tuple.h"
#include "mpl.h"
#include "math.h"

#include <memory>
#include <functional>
#include <vector>
#include <iostream>
#include <type_traits>
#include <iterator>
#include <sstream>


#include <nmmintrin.h>

template<typename Key, typename ... Vals>
struct HashsetTraits {
  using Hash = std::hash<Key>;
  static const size_t InitialSize = 32;
  static const size_t MinFreeSlots = 2;
  static const size_t GrowFactor = 2;
};

template <typename Key, typename ... Vals>
class Hashset {
  static const size_t Mod16 = 0x0F;

  using Traits = HashsetTraits<Key, Vals...>;

private:
  class Ctrl {
    struct Flags {
      enum : uint8_t {
        Empty = 0x80,
        Removed = 0xFE,
      };
    };

  private:
    Ctrl(const Ctrl &) = delete;
    Ctrl & operator=(const Ctrl &) = delete;

  public:
    inline Ctrl() noexcept { clear(); }

    inline Bitset<uint32_t> match(uint8_t v) const noexcept {
    const __m128i m = _mm_set1_epi8(v);
      return _mm_movemask_epi8(_mm_cmpeq_epi8(m, sse));
    }

    inline Bitset<uint32_t> empty() const noexcept {
    static const __m128i empty =  _mm_set1_epi8(Flags::Empty);
      return _mm_movemask_epi8(_mm_cmpeq_epi8(empty, sse));
    }
    inline Bitset<uint32_t> removed() const noexcept {
    static const __m128i removed =  _mm_set1_epi8(Flags::Removed);
      return _mm_movemask_epi8(_mm_cmpeq_epi8(removed, sse));
    }
    inline Bitset<uint32_t> emptyOrRemoved() const noexcept {
      return _mm_movemask_epi8(sse);
    }
    inline Bitset<uint32_t> assigned() const noexcept {
      return _mm_movemask_epi8(~sse);
    }
  
    inline void clear() noexcept {
    static const __m128i empty =  _mm_set1_epi8(Flags::Empty);
    sse = empty;
    }
    inline void assign(size_t i, uint8_t v) noexcept {
      data[i] = v & (~Flags::Empty);
    }
    inline void clear(size_t i) noexcept {
      data[i] = Flags::Empty;
    }
    inline void remove(size_t i) noexcept {
      data[i] = Flags::Removed;
    }

  private:
    union {
    __m128i sse;
    uint8_t data[16];
    };
  };

  using Storage = ::Storage<Ctrl, Key, Vals...>;
  template<typename T>
  using Type = typename Storage::template Type<T>;

private:
  inline Hashset(size_t n) 
    : groups_(nextPower<2>(nextMultiple<16>(n)) / 16)
    , size_(0)
    , storage_(groups_, groups_ * 16, Identity<Vals>(groups_ * 16)...)
  {
    for(size_t i = 0; i < groups(); ++i) {
      ctrls().construct(i);
    }
  }

public:
  class iterator {
    friend class Hashset<Key, Vals ...>;
    typedef Hashset<Key, Vals ...> H;
    typedef std::tuple<const Key&, Vals & ...> T; 
  public:
    typedef T value_type;
    typedef T & reference;
    typedef T* pointer;
    typedef std::forward_iterator_tag iterator_category;
    typedef int difference_type;
    inline iterator(Storage & s, size_t i) noexcept : s_(s), i_(i) {}
    inline iterator(Storage & s) noexcept : s_(s), i_(-1) { operator++(0); }
    inline iterator operator++() {
      iterator tmp = *this; operator++(0); return tmp; 
    }
    iterator & operator++(int) noexcept { 
      auto ctrls = s_.template get<Ctrl>();
      size_t n = ctrls.size();
      for(size_t j = ++i_ / 16; j < n; ++j) {
        for(auto bit : ctrls.at(j).assigned()) {
          if(j * 16 + bit >= i_) {
            i_ = j * 16 + bit;
            return *this;
          }
        }
      }
      i_ = n * 16;
      return *this; 
    }
    inline value_type operator*() noexcept { return s_.get(H::indices(), i_); }
    inline value_type operator->() noexcept { return s_.get(H::indices(), i_); }
    inline bool operator==(const iterator& rhs) const noexcept { 
        return i_ == rhs.i_ && &s_ == &rhs.s_; 
      }
    inline bool operator!=(const iterator& rhs) const noexcept { 
      return !operator==(rhs); 
    }

  private:
    Storage & s_;
    size_t i_;
  };
  class const_iterator {
    friend class Hashset<Key, Vals ...>;
    typedef Hashset<Key, Vals ...> H;
    typedef std::tuple<const Key&, const Vals & ...> T; 
  public:
    typedef T value_type;
    typedef T & reference;
    typedef T* pointer;
    typedef std::forward_iterator_tag iterator_category;
    typedef int difference_type;
    inline const_iterator(const Storage & s, size_t i) noexcept : s_(s), i_(i) {}
    inline const_iterator(const Storage & s) noexcept : s_(s), i_(-1) { operator++(0); }
    inline const_iterator operator++() { 
      const_iterator tmp = *this; operator++(0); return tmp; 
    }
    const_iterator & operator++(int) noexcept { 
      auto ctrls = s_.template get<Ctrl>();
      size_t n = ctrls.size();
      ++i_;
      for(size_t j = i_ / 16; j < n; ++j) {
        for(auto bit : ctrls.at(j).assigned()) {
          if(j * 16 + bit >= i_) {
            i_ = j * 16 + bit;
            return *this;
          }
        }
      }
      i_ = n * 16;
      return *this; 
    }
    inline value_type operator*() noexcept { return s_.get(H::indices(), i_); }
    inline value_type operator->() noexcept { return s_.get(H::indices(), i_); }
    inline bool operator==(const const_iterator& rhs) const noexcept { 
      return i_ == rhs.i_ && &s_ == &rhs.s_; 
    }
    inline bool operator!=(const const_iterator& rhs) const noexcept {
      return !operator==(rhs); 
    }

  private:
    const Storage & s_;
    size_t i_;
  };

public:
  inline Hashset() : Hashset(Traits::InitialSize) { }

  inline ~Hashset() {
    size_t i = 0;
    for(const auto & ctrl : ctrls()) {
      for(auto bit : ctrl.assigned()) {
        storage_.destroy(indices(), i + bit);
      }
      i += 16;
    }
  }

  inline Hashset(const Hashset & rhs) 
    : Hashset(round(rhs.loadFactor() * rhs.capacity()) + Traits::MinFreeSlots)
  {
    for(const auto & t : rhs) {
      insert(t);
    }
  }

  inline Hashset(Hashset && rhs) noexcept 
    : groups_(rhs.groups_)
    , size_(rhs.size_)
    , storage_(std::move(rhs.storage_))
  {

  }

  inline Hashset & operator=(Hashset rhs) {
    rhs.swap(*this);
    return *this;
  }

  bool operator==(const Hashset & rhs) const noexcept {
    for(const auto & t : rhs) {
      auto it = find(t);
      if(it == end() || t != *it) return false;
    }
    for(const auto & t : *this) {
      auto it = rhs.find(t);
      if(it == rhs.end() || t != *it) return false;
    }
    return true;
  }

  inline bool operator!=(const Hashset & rhs) const noexcept {
    return !operator==(rhs);
  }

  template<typename T, typename ... Ts>
  inline iterator find(const std::tuple<T, Ts...> & t) noexcept {
    return find(std::get<0>(t));
  }

  template<typename T, typename ... Ts>
  inline const_iterator find(const std::tuple<T, Ts...> & t) const noexcept {
    return find(std::get<0>(t));
  }

  inline iterator find(const Key &item) noexcept {
    const Hashset* t = static_cast<const Hashset*>(this);
    auto it = t->find(item);
    return iterator_at(it.i_);
  }

  const_iterator find(const Key & key) const noexcept {
    const size_t h = hash(key);
    const uint8_t h2 = H2(h);
    const size_t mask = groups() - 1;
    size_t i = H1(h) & mask;
    for(;;) {
      for(auto bit : ctrls().at(i).match(h2)) {
        size_t k = i * 16 + bit;
        if(key == keys().at(k)) {
          return iterator_at(k);
        }
      }
      if(ctrls().at(i).empty().any())
      {
        return end();
      }
      ++i;
      i &= mask;
    }
    return end();
  }

  template<typename T, typename ... Ts>
  inline iterator insert(const std::tuple<T, Ts...> & t) {
    return insert(t, IndicesFor<std::tuple<T, Ts...>>{});
  }

  template<typename T, typename ... Ts, size_t ... Is>
  inline iterator insert(const std::tuple<T, Ts...> & t, Indices<Is...>) {
    return insert(std::get<Is>(t)...); 
  }

  template<typename ... Ts>
  iterator insert(const Key & key, Ts ... ts) {
    auto it = find(key);
    if(it != end()) {
      storage_.set(indices(), it.i_, key, std::move(ts)...);
      return it;
    }
    if(loadFactor() >= MaxLoadFactor()) {
      resize(capacity() * Traits::GrowFactor);
    }
    const size_t h = hash(key);
    const uint8_t h2 = H2(h);
    const size_t mask = groups() - 1;
    size_t i = H1(h) & mask;
    for(;;) {
      for(auto bit : ctrls().at(i).emptyOrRemoved()) 
      {
        size_t k = i * 16 + bit;
        ctrls().at(i).assign(bit, h2);
        storage_.construct(indices(), k, key, std::move(ts)...);
        ++size_;
        return iterator_at(i);
      }
      ++i;
      i &= mask;
    }
  }

  bool erase(const Key &item) {
    iterator it = find(item);
    if(it != end()) {
      size_t i = it.i_;
      storage_.destroy(indices(), i);
      Ctrl & ctrl = ctrls().at(i / 16);
      if(ctrl.empty().any()) {
        ctrl.clear(i & Mod16);
        --size_;
      } else {
        ctrl.remove(i & Mod16);
      }
      return true;
    }
    return false;
  }

  inline auto begin() noexcept{ return iterator(storage_); }
  inline auto begin() const noexcept { return const_iterator(storage_); }
  
  inline auto end() noexcept { return iterator(storage_, capacity()); }
  inline auto end() const noexcept { return const_iterator(storage_, capacity()); }

  inline auto iterator_at(size_t i) noexcept { return iterator(storage_, i); }
  inline auto iterator_at(size_t i) const noexcept { return const_iterator(storage_, i); }

  inline size_t empty() const noexcept { return size_ == 0; }

  inline size_t size() const noexcept { return size_; }

  inline size_t capacity() const noexcept { return groups_ * 16; }

  inline size_t groups() const noexcept { return groups_; }

  inline float loadFactor() const noexcept { 
    return static_cast<float>(size()) / capacity(); 
  }

  inline static float MaxLoadFactor() noexcept {
  static const float maxLoadFactor = (1.0 - static_cast<float>(Traits::MinFreeSlots) / Traits::InitialSize);
    return maxLoadFactor;
  }

  inline void clear() {
    Hashset tmp;
    tmp.swap(*this);
  }

  inline void swap(Hashset & rhs) noexcept {
    using std::swap;
    swap(storage_, rhs.storage_);
    swap(groups_, rhs.groups_);
    swap(size_, rhs.size_);
  }

  void resize(size_t n) {
    Hashset tmp(n);
    const size_t nn = capacity() / 16;
    for(size_t i = 0; i < nn; ++i) {
      for(auto bit : ctrls().at(i).assigned()) {
        tmp.insert(storage_.get(indices(), i * 16 + bit));
      }
      ctrls().at(i).clear();
    }
    swap(tmp);
  }

private:
  inline Type<Ctrl> & ctrls() noexcept { return storage_.template get<Ctrl>(); }
  inline const Type<Ctrl> & ctrls() const noexcept { return storage_.template get<Ctrl>(); }

  inline Type<Key> & keys() noexcept { return storage_.template get<Key>(); }
  inline const Type<Key> & keys() const noexcept { return storage_.template get<Key>(); }

  inline size_t H1(size_t h) const noexcept {
    return h >> 7;
  }
  inline uint8_t H2(size_t h) const noexcept {
    return h & 0x7F;
  }

  inline static auto indices() noexcept {
  return Storage::template slice<1, 1+sizeof...(Vals)>();
  }

  inline static size_t hash(const Key &item) noexcept {
    static const typename Traits::Hash fn;
    return fn(item);
  }

private:
  size_t groups_;
  size_t size_;
  Storage storage_;
};

template<typename ... Ts>
std::ostream & operator<< (std::ostream & s, const Hashset<Ts...> & h) {
  s << "{";
  bool delim = false;
  for(const auto & t : h)
  {
    if(delim) s << ", ";
    delim = true;
    s << t;
  }
  s << "}";
  return s;
}
