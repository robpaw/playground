#pragma once

#include <stdexcept>
#include <cstdint>

template<typename T>
class Bitset;

template<>
class Bitset<uint32_t> {

  using This = Bitset<uint32_t>;

  static const uint32_t One = 1;
  static const uint32_t Zero = 0;

  public:

  class Iterator {

    friend class Bitset<uint32_t>;

    private:
    Iterator(uint32_t val) 
      : bit_(lsb(val))
      , val_(val & ~(1 << bit_))
    { 
    
    }

  public:
    uint32_t operator*() const { return bit_; }

    uint32_t operator++(int) { 
      uint32_t old = bit_;
      operator++();
      return old;
    }

    uint32_t operator++() { 
      bit_ = lsb(val_);
      val_ &= ~(1 << bit_);
      return bit_;
    }

    bool operator==(const Iterator & other) const {
      return val_ == other.val_ && bit_ == other.bit_;
    }

    bool operator!=(const Iterator & other) const {
      return !operator==(other);
    }

  private:
    uint32_t lsb(uint32_t v) const { return __builtin_ctz(v | 0x10000000); }

  private:
    uint32_t bit_;
    uint32_t val_;
  };

  using const_iterator = const Iterator; 

public:
  Bitset(uint32_t val) 
    : val_(val) {}

  const_iterator begin() const {
    return Iterator(val_);
  }

  const_iterator end() const {
    return Iterator(0);
  }

  bool operator==(const This & other) const {
    return val_ == other.val_;
  }

  bool operator!=(const This & other) const {
    return !operator==(other);
  }

  bool any() const { 
    return val_ != 0;
  }

  bool all() const { 
    return ~val_ == 0;
  }

  bool lsb(uint32_t & v) const {
    if(val_ != 0) {
      v = __builtin_ctz(val_);
      return true;
    }
    return false;
  }

  uint32_t lsb() const {
    if(val_ != 0) {
      return __builtin_ctz(val_);
    }
    throw std::runtime_error("value must not be 0");
  }

  bool msb(uint32_t & v) const {
    if(val_ != 0) {
      v = __builtin_clz(val_);
      return true;
    }
    return false;
  }

  uint32_t msb() const {
    if(val_ != 0) {
      return __builtin_clz(val_);
    }
    throw std::runtime_error("value must not be 0");
  }

  uint32_t count() const {
    return __builtin_popcount(val_);
  }

  uint32_t bits() const {
    return val_;
  }

  bool isSet(uint32_t i) const {
    return (val_ & (One << i)) != 0;
  }

  bool isClear(uint32_t i) const {
    return (val_ & (One << i)) == 0;
  }

  This & set() {
    val_ = ~Zero;
    return *this;
  }

  This & set(uint32_t i) {
    val_ |= (One << i);
    return *this;
  }

  This & clear() {
    val_ = Zero;
    return *this;
  }

  This & clear(uint32_t i) {
    val_ &= ~(One << i);
    return *this;
  }

  This & toggle() {
    val_ ^= val_;
    return *this;
  }

  This & toggle(uint32_t i) {
    val_ ^= (One << i);
    return *this;
  }

  This & reverse() {
    static const uint8_t BitReverseTable256[256] = {
      #define R2(n)   n,   n + 2*64,   n + 1*64,   n + 3*64
      #define R4(n) R2(n), R2(n + 2*16), R2(n + 1*16), R2(n + 3*16)
      #define R6(n) R4(n), R4(n + 2*4 ), R4(n + 1*4 ), R4(n + 3*4 )
      R6(0), R6(2), R6(1), R6(3)
    };
    uint32_t tmp;
    uint8_t * p = (unsigned char *) &val_;
    uint8_t * q = (unsigned char *) &tmp;
    q[3] = BitReverseTable256[p[0]]; 
    q[2] = BitReverseTable256[p[1]]; 
    q[1] = BitReverseTable256[p[2]]; 
    q[0] = BitReverseTable256[p[3]];
    val_ = tmp;
    return *this;
  }

  This & operator|=(const This & other) {
    val_ |= other.val_;
    return *this;
  }

  This & operator&=(const This & other) {
    val_ &= other.val_;
    return *this;
  }

  This & operator^=(const This & other) {
    val_ ^= other.val_;
    return *this;
  }

private:
  uint32_t val_;
};

template<typename T>
auto operator|(const Bitset<T> & b1, const Bitset<T> & b2) {
  auto b(b1);
  b |= b2;
  return b;
}

template<typename T>
auto operator&(const Bitset<T> & b1, const Bitset<T> & b2) {
  auto b(b1);
  b &= b2;
  return b;
}

template<typename T>
auto operator^(const Bitset<T> & b1, const Bitset<T> & b2) {
  auto b(b1);
  b ^= b2;
  return b;
}

template<typename T>
auto operator~(const Bitset<T> & b) {
  auto bb(b);
  return bb.toggle();
}

