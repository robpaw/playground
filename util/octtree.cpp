
#include <cstdint>
#include <vector>
#include <map>
#include <cmath>
#include <iostream>
#include <algorithm>

template<typename T>
struct AABB {
    std::array<T, 3> mid;
    std::array<T, 3> hlf;
    bool intersects(const AABB& rhs) const {
        return std::fabs(mid[0] - rhs.mid[0]) < (hlf[0] + rhs.hlf[0])
            && std::fabs(mid[1] - rhs.mid[1]) < (hlf[1] + rhs.hlf[1])
            && std::fabs(mid[2] - rhs.mid[2]) < (hlf[2] + rhs.hlf[2]);
    }
    bool encloses(const AABB& rhs) const {
        return std::fabs(mid[0] - rhs.mid[0]) <= (hlf[0] - rhs.hlf[0])
            && std::fabs(mid[1] - rhs.mid[1]) <= (hlf[1] - rhs.hlf[1])
            && std::fabs(mid[2] - rhs.mid[2]) <= (hlf[2] - rhs.hlf[2]);
    }
    bool encloses(T x, T y, T z) const {
        return std::fabs(mid[0] - x) <= hlf[0]
            && std::fabs(mid[1] - y) <= hlf[1]
            && std::fabs(mid[2] - z) <= hlf[2];
    }
    void expand(const AABB & rhs) {
        hlf[0] = std::max<T>(std::fabs(mid[0] - rhs.mid[0]) + rhs.hlf[0], hlf[0]);
        hlf[1] = std::max<T>(std::fabs(mid[1] - rhs.mid[1]) + rhs.hlf[1], hlf[1]);
        hlf[2] = std::max<T>(std::fabs(mid[2] - rhs.mid[2]) + rhs.hlf[2], hlf[2]);
    }
    void scale(T s) {
        hlf[0] *= s;
        hlf[1] *= s;
        hlf[2] *= s;
    }
};

template<typename UserType, typename CoordType = double>
class OctTree {
public:
    using I = uint16_t;
    using T = CoordType;
    using U = UserType;
    using B = AABB<T>;
    using UList = std::vector<U>;

private:
    struct Node {
        I parent;
        std::array<I, 8> children;
        UList data;
    };

public:
    OctTree(B b) 
        : box(b), root(1), nodes(2, {0}) 
    { }
    void insert(U u, B b) {
        while(!box.encloses(b)) {
            I childIndex = getChildIndex(b, box);

            I nn = createNode();
            Node& n = nodes[nn];
            n.parent == nn;
            n.children[childIndex] = root;
            nodes[root].parent = nn;
            root = nn;

            box.mid[0] += (childIndex & 1) ? -box.hlf[0] : box.hlf[0];
            box.mid[1] += (childIndex & 2) ? -box.hlf[1] : box.hlf[1];
            box.mid[2] += (childIndex & 4) ? -box.hlf[2] : box.hlf[2];
            box.scale(2);
        } 
       
        I currIndex = root;
        B bb(box);
        for(;;) {
            I childIndex = getChildIndex(bb, b);
            
            bb.scale(0.5);
            bb.mid[0] += (childIndex & 1) ? bb.hlf[0] : -bb.hlf[0];
            bb.mid[1] += (childIndex & 2) ? bb.hlf[1] : -bb.hlf[1];
            bb.mid[2] += (childIndex & 4) ? bb.hlf[2] : -bb.hlf[2];

            if(!bb.encloses(b)) {
                nodes[currIndex].data.push_back(u);
                dataToNode[u].push_back(currIndex);
                return;
            }

            if(nodes[currIndex].children[childIndex] == 0) {
                I nn = createNode();
                Node& n = nodes[nn];
                n.parent = currIndex;
                nodes[currIndex].children[childIndex] = nn;
            }
            currIndex = nodes[currIndex].children[childIndex];
        }
    }
    void remove(U u) {
       for(I i : dataToNode[u]) {
           Node& n = nodes[i];
           n.data.erase(std::remove(n.data.begin(), n.data.end(), u), n.data.end());
           cleanup(i);
        }
        dataToNode.erase(u);
    }
    UList find(B b) const {
        I currIndex = root, prevIndex = root;
        B bb(box), bb2;
        do {
            I childIndex = getChildIndex(bb, b);
            bb2 = bb;
            bb.scale(0.5);
            bb.mid[0] += (childIndex & 1) ? bb.hlf[0] : -bb.hlf[0];
            bb.mid[1] += (childIndex & 2) ? bb.hlf[1] : -bb.hlf[1];
            bb.mid[2] += (childIndex & 4) ? bb.hlf[2] : -bb.hlf[2];
            prevIndex = currIndex;
            nodes[currIndex].children[childIndex];
        } while(bb.encloses(b) && currIndex != 0);
        UList data;
        collect(prevIndex, data);
        return data;
    }
private:
    void cleanup(I i) {
        Node& n = nodes[i];
        if(n.data.empty() && n.parent != root) {
            if(std::count(n.children.begin(), n.children.end(), 0) == 8) {
                for(I& ii : nodes[n.parent].children) {
                    if(ii == i) {
                        ii = 0;
                        break;
                    }
                }
                I k = n.parent;
                n.parent = 0;
                freeNodes.push_back(i);
                cleanup(k);
            }
        }
    }
    I createNode() {
        if(freeNodes.empty()) {
            I i = nodes.size();
            nodes.push_back({});
            return i;
        }
        I i = freeNodes.back();
        freeNodes.pop_back();
        return i;
    }
    I getChildIndex(const B& parent, const B& child) const {
        I cell = 0;
        cell |= (child.mid[0] > parent.mid[0] ? 1 : 0) << 0;
        cell |= (child.mid[1] > parent.mid[1] ? 1 : 0) << 1;
        cell |= (child.mid[2] > parent.mid[2] ? 1 : 0) << 2;
        return cell;
    }
    void collect(I nodeIndex, UList& list) const {
        const Node& n = nodes[nodeIndex];
        std::copy(n.data.begin(), n.data.end(), std::back_inserter(list));
        for(I i : n.children) {
            if(i != 0) {
                collect(i, list);
            }
        }
    }

private:
    B box;
    I root;
    std::vector<Node> nodes;
    std::map<U, std::vector<I>> dataToNode;
    std::vector<I> freeNodes;
};


int main(int argc, char ** argv) {
    
    AABB<double> a, b, c;

    a.mid[0] = 0;
    a.mid[1] = 0;
    a.mid[2] = 0;
    a.hlf[0] = 1;
    a.hlf[1] = 1;
    a.hlf[2] = 1;

    b.mid[0] = 0.2;
    b.mid[1] = 0.2;
    b.mid[2] = 0.2;
    b.hlf[0] = 0.1;
    b.hlf[1] = 0.1;
    b.hlf[2] = 0.1;

    c.mid[0] = 1;
    c.mid[1] = 1;
    c.mid[2] = 1;
    c.hlf[0] = 2;
    c.hlf[1] = 2;
    c.hlf[2] = 2;

    OctTree<int> t(a);
    std::cout << "insert" << std::endl;
    t.insert(1, b);
    for(auto i : t.find(c)) {
        std::cout << i << std::endl;
    }
    for(auto i : t.find(b)) {
        std::cout << i << std::endl;
    }
    std::cout << "insert" << std::endl;
    t.insert(2, c);
    for(auto i : t.find(c)) {
        std::cout << i << std::endl;
    }
    for(auto i : t.find(b)) {
        std::cout << i << std::endl;
    }
}
