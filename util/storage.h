#pragma once

#include "mpl.h"
#include "memory.h"

#include <algorithm>
#include <iterator>
#include <memory>
#include <numeric>
#include <tuple>
#include <iostream>

#include <cassert>

template<typename ... Args>
struct StorageTraits {
  static const size_t Alignment = 64;
};

template<typename ... Args>
class Storage {

  using byte_t = uint8_t;
  using Tuple = std::tuple<Args...>;
  using Traits = StorageTraits<Args...>;

public:
  template<typename T>
  struct Type {
    friend class Storage<Args...>;
    using count_t = size_t;
    inline Type(count_t count) noexcept : count_(count), ptr_(nullptr) { }

  public:
    using iterator = T*;
    using const_iterator = const T*;

    inline static size_t aligment() noexcept {
      return alignof(T) > Traits::Alignment ? alignof(T) : Traits::Alignment;
    }
    inline size_t bytes() const noexcept { return count_ * sizeof(T) + aligment(); }
    inline size_t size() const noexcept { return count_; }
    inline T & at(size_t i) noexcept { return ptr_[i]; }
    inline const T & at(size_t i) const noexcept { return ptr_[i]; }

    template<typename ... A>
    inline void construct(size_t i, A && ... a) { new(ptr_ + i) T(std::forward<A>(a)...); }
    inline void destroy(size_t i) { (ptr_ + i)->~T(); }

    inline iterator begin() noexcept { return ptr_; }
    inline const_iterator begin() const noexcept { return ptr_; }

    inline iterator end() noexcept { return ptr_ + count_; }
    inline const_iterator end() const noexcept { return ptr_ + count_; }

  private:
    inline void align(void *& mem, size_t & space) {
      void * p = ::align(aligment(), size(), mem, space);
      ptr_ = reinterpret_cast<T*>(p);
    }

  private:
    size_t count_;
    T * ptr_;
  };

private:
  template<typename Tuple, typename Fn, size_t ... Is>
  inline static void ForEach(Tuple & t, Fn fn, Indices<Is...>) {
    Expand{(fn(std::get<Is>(t)), 0)...};
  }
  template<typename Tuple, typename Fn>
  inline static void ForEach(Tuple & t, Fn fn) {
    ForEach(t, fn, IndicesFor<Tuple>{});
  }

private:
  Storage(const Storage & rhs) = delete;
  Storage & operator=(const Storage & rhs) = delete;

public:
  inline Storage(typename Type<Args>::count_t ... count)
    : ptrs_(count...)
  {
    size_t space = 0;
    ForEach(ptrs_, [&](auto & t) { space += t.bytes(); });
    mem_ = new byte_t[space];
    void * p = mem_;
    ForEach(ptrs_, [&](auto & t) { t.align(p, space); });
  }
  inline Storage(Storage && rhs) noexcept
    : ptrs_(rhs.ptrs_)
    , mem_(rhs.mem_)
  {
    rhs.mem_ = nullptr;
  }
  inline Storage & operator=(Storage && rhs) noexcept {
    auto copy(std::move(rhs));
    copy.swap(*this);
    return *this;
  }
  inline ~Storage() {
    delete mem_;
  }

  inline static auto indices() noexcept {
    return IndicesFor<std::tuple<Args...>>{};
  }
  template<size_t ... Is>
  inline static auto indices() noexcept {
    return Indices<Is...>{};
  }
  template<typename ... Ts>
  inline static auto indices() noexcept {
    return Indices<ToIndex<Ts, Tuple>::value...>{};
  }
  template<size_t From, size_t To>
  inline static auto slice() noexcept {
    static_assert(To < sizeof...(Args), "invalid range");
    return Slice<From, To>{};
  }

  template<typename ... Ts, size_t ... Is>
  void construct(Indices<Is...> is, size_t i, Ts && ... ts) {
    Expand{ (std::get<Is>(ptrs_).construct(i, std::forward<Ts>(ts)), 0)...};
  }
  template<size_t ... Is>
  void destroy(Indices<Is...> is, size_t i) {
    Expand{ (std::get<Is>(ptrs_).destroy(i), 0)...};
  }

  template<typename T>
  inline auto & get() noexcept { return std::get<Type<T>>(ptrs_); }
  template<typename T>
  inline const auto & get() const noexcept { return std::get<Type<T>>(ptrs_); }

  template<size_t I>
  inline auto & get() noexcept { return std::get<I>(ptrs_); }
  template<size_t I>
  inline const auto & get() const noexcept { return std::get<I>(ptrs_); }

  template<size_t ... Is>
  inline auto get(Indices<Is...> is, size_t i) noexcept {
    return std::tuple<ToType<Is, Tuple> & ...>(std::get<Is>(ptrs_).at(i) ...);
  }
  template<size_t ... Is>
  inline auto get(Indices<Is...> is, size_t i) const noexcept {
    return std::tuple<const ToType<Is, Tuple>& ...>(std::get<Is>(ptrs_).at(i) ...);
  }

  template<typename ... Ts, size_t ... Is>
  inline void set(Indices<Is...> is, size_t i, Ts && ... ts) {
    Expand{(std::get<Is>(ptrs_).at(i) = std::move(ts), 0) ...};
  }

  template<typename ... Ts, size_t ... Is>
  inline void set(Indices<Is...> is, size_t i, const std::tuple<Ts...> & t) {
    get(is, i) = t;
  }

  inline void swap(Storage<Args...> & rhs) noexcept {
    std::swap(ptrs_, rhs.ptrs_);
    std::swap(mem_, rhs.mem_);
  }

  friend inline void swap(Storage & lhs, Storage & rhs) noexcept {
    lhs.swap(rhs);
  }

public:
  template<size_t ... Is>
  class Iterator
    : public std::iterator<std::random_access_iterator_tag, std::tuple< ToType<Is, std::tuple<Args...>> & ... > >
  {
    friend class Storage<Args...>;

  private:
    using Type = std::tuple<ToType<Is, std::tuple<Args...>> & ...>;

  public:
    using value_type = Type;
    using difference_type = typename std::iterator<std::random_access_iterator_tag, Type>::difference_type;

  private:
    inline Iterator(Storage<Args...>* storage, size_t i) : s_(storage), i_(i) {}

  public:
    inline Iterator() noexcept : s_(nullptr), i_(0) {}
    inline Iterator(const Iterator &rhs) noexcept : s_(rhs.s_), i_(rhs.i_) {}

    inline Iterator& operator+=(difference_type rhs) noexcept {i_ += rhs; return *this;}
    inline Iterator& operator-=(difference_type rhs) noexcept {i_ -= rhs; return *this;}
    inline Type operator*() noexcept { return s_->get(Indices<Is...>{}, i_); }
    inline Type operator[](difference_type rhs) noexcept {return s_->get(Indices<Is...>{}, i_+rhs);}

    inline Iterator& operator++() noexcept {++i_; return *this;}
    inline Iterator& operator--() noexcept {--i_; return *this;}
    inline Iterator operator++(int) noexcept {Iterator tmp(*this); ++i_; return tmp;}
    inline Iterator operator--(int) noexcept {Iterator tmp(*this); --i_; return tmp;}
    inline difference_type operator-(const Iterator& rhs) const noexcept {return i_-rhs.i_; }
    inline Iterator operator+(difference_type rhs) const noexcept {return Iterator(s_, i_+rhs);}
    inline Iterator operator-(difference_type rhs) const noexcept {return Iterator(s_, i_-rhs);}
    friend inline Iterator operator+(difference_type lhs, const Iterator& rhs) noexcept {return Iterator(rhs.s_, lhs+rhs.i_);}
    friend inline Iterator operator-(difference_type lhs, const Iterator& rhs) noexcept {return Iterator(rhs.s_, lhs-rhs.i_);}

    inline bool operator==(const Iterator& rhs) const noexcept {return i_ == rhs.i_;}
    inline bool operator!=(const Iterator& rhs) const noexcept {return i_ != rhs.i_;}
    inline bool operator>(const Iterator& rhs) const noexcept {return i_ > rhs.i_;}
    inline bool operator<(const Iterator& rhs) const noexcept {return i_ < rhs.i_;}
    inline bool operator>=(const Iterator& rhs) const noexcept {return i_ >= rhs.i_;}
    inline bool operator<=(const Iterator& rhs) const noexcept {return i_ <= rhs.i_;}

    friend void iter_swap(Iterator lhs, Iterator rhs) noexcept {
      std::swap(stay(*lhs), stay(*rhs));
    }

  private:
    Storage<Args...>* s_;
    size_t i_;
  };

  template<size_t ... Is>
  class ConstIterator
    : public std::iterator<std::random_access_iterator_tag, std::tuple<const ToType<Is, Tuple> & ... >>
  {
    friend class Storage<Args...>;

    using Type = std::tuple<const ToType<Is, Tuple> & ... >;

  public:
    using value_type = Type;
    using difference_type = typename std::iterator<std::random_access_iterator_tag, Type>::difference_type;

  private:
    inline ConstIterator(const Storage<Args...>* storage, size_t i) : s_(storage), i_(i) {}

  public:
    inline ConstIterator() noexcept : s_(nullptr), i_(0) {}
    inline ConstIterator(const ConstIterator &rhs) noexcept : s_(rhs.s_), i_(rhs.i_) {}

    inline ConstIterator& operator+=(difference_type rhs) noexcept {i_ += rhs; return *this;}
    inline ConstIterator& operator-=(difference_type rhs) noexcept {i_ -= rhs; return *this;}
    inline Type operator*() const noexcept {return s_->get(Indices<Is...>{}, i_);}
    inline Type operator[](difference_type rhs) const noexcept {return s_->get(Indices<Is...>{}, i_+rhs);}

    inline ConstIterator& operator++() noexcept {++i_; return *this;}
    inline ConstIterator& operator--() noexcept {--i_; return *this;}
    inline ConstIterator operator++(int) noexcept {ConstIterator tmp(*this); ++i_; return tmp;}
    inline ConstIterator operator--(int) noexcept {ConstIterator tmp(*this); --i_; return tmp;}
    inline difference_type operator-(const ConstIterator& rhs) const noexcept {return i_-rhs.i_;}
    inline ConstIterator operator+(difference_type rhs) const noexcept {return ConstIterator(s_, i_+rhs);}
    inline ConstIterator operator-(difference_type rhs) const noexcept {return ConstIterator(s_, i_-rhs);}
    friend inline ConstIterator operator+(difference_type lhs, const ConstIterator& rhs) noexcept {return ConstIterator(rhs.s_, lhs+rhs.i_);}
    friend inline ConstIterator operator-(difference_type lhs, const ConstIterator& rhs) noexcept {return v(rhs.s_, lhs-rhs.i_);}

    inline bool operator==(const ConstIterator& rhs) const noexcept {return i_ == rhs.i_;}
    inline bool operator!=(const ConstIterator& rhs) const noexcept {return i_ != rhs.i_;}
    inline bool operator>(const ConstIterator& rhs) const noexcept {return i_ > rhs.i_;}
    inline bool operator<(const ConstIterator& rhs) const noexcept {return i_ < rhs.i_;}
    inline bool operator>=(const ConstIterator& rhs) const noexcept {return i_ >= rhs.i_;}
    inline bool operator<=(const ConstIterator& rhs) const noexcept {return i_ <= rhs.i_;}
  private:
    const Storage<Args...>* s_;
    size_t i_;
  };

public:
  inline auto begin() noexcept { return begin(IndicesFor<Tuple>{}); }
  inline auto begin() const noexcept { return begin(IndicesFor<Tuple>{}); }
  template<size_t ... Is>
  inline auto begin(Indices<Is...>) noexcept { return Iterator<Is...>(this, 0); }
  template<size_t ... Is>
  inline auto begin(Indices<Is...>) const noexcept { return ConstIterator<Is...>(this, 0); }

  inline auto end() noexcept { return end(IndicesFor<Tuple>{}); }
  inline auto end() const noexcept { return end(IndicesFor<Tuple>{}); }
  template<size_t ... Is>
  inline auto end(Indices<Is...> is) noexcept { return Iterator<Is...>(this, minSize(is)); }
  template<size_t ... Is>
  inline auto end(Indices<Is...> is) const noexcept { return ConstIterator<Is...>(this, minSize(is)); }

private:
  template<size_t ... Is>
  inline size_t minSize(Indices<Is...>) noexcept {
    return std::min( { get<Is>().size()... } );
  }

private:
  std::tuple<Type<Args>...> ptrs_;
  byte_t * mem_;
};


