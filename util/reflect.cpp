
#include <iostream>

#include <string>
#include <vector>
#include <map>
#include <stdexcept>
#include <typeinfo>
#include <algorithm>

#include <cstdint>
#include <cassert>

template<typename T>
class TypeTraits {
    static std::string n;

public:
    static uint64_t uuid() {
        return reinterpret_cast<uint64_t>(&n);
    }
    static const std::string& name() {
        return n;
    }
};

template<typename T>
std::string TypeTraits<T>::n = typeid(T).name();

class Any {
    struct Base {
        virtual ~Base() {}
        virtual Base* clone() const = 0;
        virtual uint64_t uuid() const = 0;
        virtual std::string typeName() const = 0;
    };
    template<typename T>
    struct Type : Base {
        Type(const T& t) : t(t) {}
        Type(T&& t) : t(std::move(t)) {}
        template<typename ... Args>
        Type(Args ... args) : t(std::forward<Args>(args)...) {}
        Base* clone() const { return new Type<T>(t); }
        uint64_t uuid() const { return TypeTraits<typename std::decay<T>::type>::uuid(); }
        virtual std::string typeName() const { return typeid(T).name(); };

        T t;
    };

public:
    Any() : base(nullptr) { }

    template<typename T>
    Any(T t) : base(new Type<typename std::decay<T>::type>(std::forward<T>(t))) {}

    Any(const Any& rhs) : base(rhs.base->clone()) {}
    Any(Any && rhs) : base(rhs.base) { rhs.base = nullptr; }

    ~Any() { delete base; }

    Any& operator=(Any rhs) { swap(rhs); return *this; }

    template<typename T>
    bool is() const {
        return TypeTraits<typename std::decay<T>::type>::uuid() == base->uuid();
    }

    template<typename T>
    T& cast() {
        using U = typename std::decay<T>::type;
        if(!is<U>()) {
            std::string err;
            err += "invalid cast; expected ";
            err += base->typeName();
            err += " got ";
            err += typeid(U).name();
            throw std::runtime_error(err.c_str());
        }
        return static_cast<Type<U>*>(base)->t;
    }

    template<typename T>
    const T& cast() const {
        using U = typename std::decay<T>::type;
        if(!is<U>()) {
            std::string err;
            err += "invalid cast; expected ";
            err += base->typeName();
            err += " got ";
            err += typeid(U).name();
            throw std::runtime_error(err.c_str());
        }
        return static_cast<Type<U>*>(base)->t;
    }

    void swap(Any& rhs) { std::swap(base, rhs.base); }

private:
    Base* base;
};

namespace {
    template<typename InputIt1, typename InputIt2>
    bool equal(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2) {
        for(; first1 != last1 && first2 != last2; ++first1, ++first2) {
            if(!(*first1 == *first2)) {
                return false;
            }
        }
        return first1 == last1;
    }
    template<typename InputIt1, typename InputIt2, typename BinaryPredicate>
    bool equal(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, BinaryPredicate p) {
        for(; first1 != last1 && first2 != last2; ++first1, ++first2) {
            if(!p(*first1, *first2)) {
                return false;
            }
        }
        return first1 == last1;
    }
    template<typename T>
    bool operator==(const std::vector<T>& lhs, std::vector<T>& rhs) {
        return ::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }
    template<typename T, typename U>
    bool operator==(const std::map<T, U>& lhs, std::map<T, U>& rhs) {
        return ::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }
    template<typename T>
    bool operator<(const std::vector<T>& lhs, std::vector<T>& rhs) {
        return ::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::less<T>());
    }
    template<typename T, typename U>
    bool operator<(const std::map<T, U>& lhs, std::map<T, U>& rhs) {
        return ::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), std::less<std::pair<T, U>>());
    }
}

class Variant
{
    union Storage {
        char ch;
        bool b;
        int8_t i8;
        uint8_t u8;
        int16_t i16;
        uint16_t u16;
        int32_t i32;
        uint32_t u32;
        int64_t i64;
        uint64_t u64;
        float f32;
        double f64;
        std::string* str;
        std::vector<Variant>* vec;
        std::map<Variant, Variant>* map;
        Any* any;
    };
    enum Type : uint8_t {
        TYPE_NONE = 0x00,
        TYPE_BOOL = 0x10,
        TYPE_CHAR,
        TYPE_U8,
        TYPE_U16,
        TYPE_U32,
        TYPE_U64,
        TYPE_I8,
        TYPE_I16,
        TYPE_I32,
        TYPE_I64,
        TYPE_F32 = 0x20,
        TYPE_F64,
        TYPE_STRING = 0x40,
        TYPE_VECTOR,
        TYPE_MAP,
        TYPE_ANY,
    };

public:
    using String = std::string;
    using Vector = std::vector<Variant>;
    using Map = std::map<Variant, Variant>;

public:
    Variant();
    Variant(char);
    Variant(bool);
    Variant(int8_t);
    Variant(uint8_t);
    Variant(int16_t);
    Variant(uint16_t);
    Variant(int32_t);
    Variant(uint32_t);
    Variant(int64_t);
    Variant(uint64_t);
    Variant(float);
    Variant(double);
    Variant(const char*);
    Variant(String);
    Variant(Vector);
    Variant(Map);
    Variant(Any);
    template<typename T>
    Variant(T t) : type(TYPE_ANY) {
        Any any(std::forward<T>(t));
        storage.any = new Any(std::move(any));
    }

    Variant(const Variant&);
    Variant(Variant&&);

    ~Variant();

    Variant& operator=(char);
    Variant& operator=(bool);
    Variant& operator=(int8_t);
    Variant& operator=(uint8_t);
    Variant& operator=(int16_t);
    Variant& operator=(uint16_t);
    Variant& operator=(int32_t);
    Variant& operator=(uint32_t);
    Variant& operator=(int64_t);
    Variant& operator=(uint64_t);
    Variant& operator=(float);
    Variant& operator=(double);
    Variant& operator=(String);
    Variant& operator=(Vector);
    Variant& operator=(Map);
    Variant& operator=(Any);
    Variant& operator=(Variant);

    bool isEmpty() const;
    bool isBool() const;
    bool isIntegral() const;
    bool isFloating() const;
    bool isNumber() const;
    bool isString() const;
    bool isVector() const;
    bool isMap() const;
    bool isAny() const;
    bool isComplex() const;

    void swap(Variant&) noexcept;

    bool operator==(const Variant&) const;
    bool operator!=(const Variant&) const;
    bool operator<(const Variant&) const;

    explicit operator char() const;
    explicit operator bool() const;
    explicit operator int8_t() const;
    explicit operator uint8_t() const;
    explicit operator int16_t() const;
    explicit operator uint16_t() const;
    explicit operator int32_t() const;
    explicit operator uint32_t() const;
    explicit operator int64_t() const;
    explicit operator uint64_t() const;
    explicit operator float() const;
    explicit operator double() const;

    explicit operator String&();
    explicit operator Vector&();
    explicit operator Map&();
    explicit operator Any&();
    template<typename T, typename = typename std::enable_if<!std::is_fundamental<T>::value>::type>
    explicit operator T&() { return static_cast<Any&>(*this).cast<T&>(); }

    explicit operator const String&() const;
    explicit operator const Vector&() const;
    explicit operator const Map&() const;
    explicit operator const Any&() const;
    template<typename T, typename = typename std::enable_if<!std::is_fundamental<T>::value>::type>
    explicit operator const T&() const { return static_cast<const Any&>(*this).cast<const T&>(); }

private:
    bool isEqual(const Variant&) const;

    template<typename T>
    T cast() const {
        switch(type) {
            case TYPE_CHAR: return storage.ch;
            case TYPE_BOOL: return storage.b;
            case TYPE_I8: return storage.i8;
            case TYPE_U8: return storage.u8;
            case TYPE_I16: return storage.i16;
            case TYPE_U16: return storage.u16;
            case TYPE_I32: return storage.i32;
            case TYPE_U32: return storage.u32;
            case TYPE_I64: return storage.i64;
            case TYPE_U64: return storage.u64;
            case TYPE_F32: return storage.f32;
            case TYPE_F64: return storage.f64;
            default: throw std::runtime_error("invalid cast");
        }
    }

private:
    Storage storage;
    Type type;
};

Variant::Variant() : type(TYPE_NONE) {}
Variant::Variant(char v) : type(TYPE_CHAR) { storage.ch = v; }
Variant::Variant(bool v) : type(TYPE_BOOL) { storage.b = v; }
Variant::Variant(int8_t v) : type(TYPE_I8) { storage.i8 = v; }
Variant::Variant(uint8_t v) : type(TYPE_U8) { storage.u8 = v; }
Variant::Variant(int16_t v) : type(TYPE_I16) { storage.i16 = v; }
Variant::Variant(uint16_t v) : type(TYPE_U16) { storage.u16 = v; }
Variant::Variant(int32_t v) : type(TYPE_I32) { storage.i32 = v; }
Variant::Variant(uint32_t v) : type(TYPE_U32) { storage.u32 = v; }
Variant::Variant(int64_t v) : type(TYPE_I64) { storage.i64 = v; }
Variant::Variant(uint64_t v) : type(TYPE_U64) { storage.u64 = v; }
Variant::Variant(float v) : type(TYPE_F32) { storage.f32 = v; }
Variant::Variant(double v) : type(TYPE_F64) { storage.f64 = v; }
Variant::Variant(const char* v) : type(TYPE_STRING) { storage.str = new String(v); }
Variant::Variant(String v) : type(TYPE_STRING) { storage.str = new String(std::move(v)); }
Variant::Variant(Vector v) : type(TYPE_VECTOR) { storage.vec = new Vector(std::move(v)); }
Variant::Variant(Map v) : type(TYPE_MAP) { storage.map = new Map(std::move(v)); }
Variant::Variant(Any v) : type(TYPE_ANY) { storage.any = new Any(std::move(v)); }

Variant::Variant(const Variant& rhs) : type(rhs.type) {
    switch(type) {
        case TYPE_STRING: storage.str = new String(*rhs.storage.str); break;
        case TYPE_VECTOR: storage.vec = new Vector(*rhs.storage.vec); break;
        case TYPE_MAP: storage.map = new Map(*rhs.storage.map); break;
        case TYPE_ANY: storage.any = new Any(*rhs.storage.any); break;
        default: storage = rhs.storage;
    }
}
Variant::Variant(Variant&& rhs) : storage(rhs.storage), type(rhs.type) { rhs.type = TYPE_NONE; }

Variant::~Variant() {
    switch(type) {
        case TYPE_STRING: delete storage.str; break;
        case TYPE_VECTOR: delete storage.vec; break;
        case TYPE_MAP: delete storage.map; break;
        case TYPE_ANY: delete storage.any; break;
        default: break;
    }
}

Variant& Variant::operator=(char rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(bool rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(int8_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(uint8_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(int16_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(uint16_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(int32_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(uint32_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(int64_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(uint64_t rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(float rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(double rhs) { Variant tmp(rhs); swap(tmp); return *this; }
Variant& Variant::operator=(String rhs) { Variant tmp(std::move(rhs)); swap(tmp); return *this; }
Variant& Variant::operator=(Vector rhs) { Variant tmp(std::move(rhs)); swap(tmp); return *this; }
Variant& Variant::operator=(Map rhs) { Variant tmp(std::move(rhs)); swap(tmp); return *this; }
Variant& Variant::operator=(Any rhs) { Variant tmp(std::move(rhs)); swap(tmp); return *this; }
Variant& Variant::operator=(Variant rhs) { swap(rhs); return *this; }

bool Variant::isEmpty() const { return type == TYPE_NONE; }
bool Variant::isBool() const { return type == TYPE_BOOL; }
bool Variant::isIntegral() const { return type & 0x10; }
bool Variant::isFloating() const { return type & 0x20; }
bool Variant::isNumber() const { return type & 0x30; }
bool Variant::isString() const { return type == TYPE_STRING; }
bool Variant::isVector() const { return type == TYPE_VECTOR; }
bool Variant::isMap() const { return type == TYPE_MAP; }
bool Variant::isAny() const { return type == TYPE_ANY; }
bool Variant::isComplex() const { return type & 0x40; }

void Variant::swap(Variant& rhs) noexcept {
    std::swap(storage, rhs.storage);
    std::swap(type, rhs.type);
}

bool Variant::operator==(const Variant& rhs) const {
    if(isComplex() && rhs.isComplex()) {
        if(type == rhs.type) {
            switch(type) {
                case TYPE_STRING: return *storage.str == *rhs.storage.str;
                case TYPE_VECTOR: return *storage.vec == *rhs.storage.vec;
                case TYPE_MAP: return *storage.map == *rhs.storage.map;
                default: return false;
            }
        }
    } else {
        return isEqual(rhs) && rhs.isEqual(*this);
    }
    return false;
}
bool Variant::operator!=(const Variant& rhs) const {
    return !operator==(rhs);
}
bool Variant::operator<(const Variant& rhs) const {
    if(type != rhs.type || isAny() || isEmpty()) {
        throw std::runtime_error("invalid comparison");
    }
    switch(type) {
        case TYPE_CHAR: return storage.ch < rhs.storage.ch;
        case TYPE_BOOL: return storage.b < rhs.storage.b;
        case TYPE_I8: return storage.i8 < rhs.storage.i8;
        case TYPE_U8: return storage.u8 < rhs.storage.u8;
        case TYPE_I16: return storage.i16 < rhs.storage.i16;
        case TYPE_U16: return storage.u16 < rhs.storage.u16;
        case TYPE_I32: return storage.i32 < rhs.storage.i32;
        case TYPE_U32: return storage.u32 < rhs.storage.u32;
        case TYPE_I64: return storage.i64 < rhs.storage.i64;
        case TYPE_U64: return storage.u64 < rhs.storage.u64;
        case TYPE_F32: return storage.f32 < rhs.storage.f32;
        case TYPE_F64: return storage.f64 < rhs.storage.f64;
        case TYPE_STRING: return *storage.str < *rhs.storage.str;
        case TYPE_VECTOR: return *storage.vec < *rhs.storage.vec;
        case TYPE_MAP: return *storage.map < *rhs.storage.map;
        default: throw std::runtime_error("invalid comparison");
     }
}

bool Variant::isEqual(const Variant& rhs) const {
    switch(type) {
        case TYPE_CHAR: return storage.ch == rhs.cast<char>();
        case TYPE_BOOL: return storage.b == rhs.cast<bool>();
        case TYPE_I8: return storage.i8 == rhs.cast<int8_t>();
        case TYPE_U8: return storage.u8 == rhs.cast<uint8_t>();
        case TYPE_I16: return storage.i16 == rhs.cast<int16_t>();
        case TYPE_U16: return storage.u16 == rhs.cast<uint16_t>();
        case TYPE_I32: return storage.i32 == rhs.cast<int32_t>();
        case TYPE_U32: return storage.u32 == rhs.cast<uint32_t>();
        case TYPE_I64: return storage.i64 == rhs.cast<int64_t>();
        case TYPE_U64: return storage.u64 == rhs.cast<uint64_t>();
        case TYPE_F32: return storage.f32 == rhs.cast<float>();
        case TYPE_F64: return storage.f64 == rhs.cast<double>();
        default: throw std::runtime_error("invalid comparison");
    }
}

Variant::operator char() const { return cast<char>(); }
Variant::operator bool() const { return cast<bool>(); }
Variant::operator int8_t() const { return cast<int8_t>(); }
Variant::operator uint8_t() const { return cast<uint8_t>(); }
Variant::operator int16_t() const { return cast<int16_t>(); }
Variant::operator uint16_t() const { return cast<uint16_t>(); }
Variant::operator int32_t() const { return cast<int32_t>(); }
Variant::operator uint32_t() const { return cast<uint32_t>(); }
Variant::operator int64_t() const { return cast<int64_t>(); }
Variant::operator uint64_t() const { return cast<uint64_t>(); }
Variant::operator float() const { return cast<float>(); }
Variant::operator double() const { return cast<double>(); }

Variant::operator Variant::String&() { if(!isString()) throw std::runtime_error("invalid cast"); return *storage.str; }
Variant::operator Variant::Vector&() { if(!isVector()) throw std::runtime_error("invalid cast"); return *storage.vec; }
Variant::operator Variant::Map&() { if(!isMap()) throw std::runtime_error("invalid cast"); return *storage.map; }
Variant::operator Any&() { if(!isAny()) throw std::runtime_error("invalid cast"); return *storage.any; }

Variant::operator const Variant::String&() const { if(!isString()) throw std::runtime_error("invalid cast"); return *storage.str; }
Variant::operator const Variant::Vector&() const { if(!isVector()) throw std::runtime_error("invalid cast"); return *storage.vec; }
Variant::operator const Variant::Map&() const { if(!isMap()) throw std::runtime_error("invalid cast"); return *storage.map; }
Variant::operator const Any&() const { if(!isAny()) throw std::runtime_error("invalid cast"); return *storage.any; }


namespace {
    struct IsConst {
        template<typename T>
        constexpr IsConst(T) : value(std::is_const<T>::value) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...)) : value(false) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const &&) : value(true) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args...) const volatile &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsConst(R (T::*)(Args..., ...) const volatile &&) : value(true) {}

        bool value;
    };
    struct IsVolatile {
        template<typename T>
        constexpr IsVolatile(T) : value(std::is_volatile<T>::value) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...)) : value(false) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) volatile &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) volatile &&) : value(true) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args...) const volatile &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsVolatile(R (T::*)(Args..., ...) const volatile &&) : value(true) {}

        bool value;
    };

    struct IsReference {
        template<typename T>
        constexpr IsReference(T) : value(std::is_reference<T>::value) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...)) : value(false) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) volatile) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) volatile &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) volatile) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) volatile &&) : value(true) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) const volatile) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args...) const volatile &&) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) const volatile) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) const volatile &) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsReference(R (T::*)(Args..., ...) const volatile &&) : value(true) {}

        bool value;
    };

    struct IsPointer {
        template<typename T>
        constexpr IsPointer(T) : value(std::is_pointer<T>::value) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...)) : value(true) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) volatile &) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) volatile &&) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) volatile &) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) volatile &&) : value(false) {}

        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) const volatile &) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args...) const volatile &&) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) const volatile) : value(true) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) const volatile &) : value(false) {}
        template<typename R, typename T, typename ... Args>
        constexpr IsPointer(R (T::*)(Args..., ...) const volatile &&) : value(false) {}

        bool value;
    };


    template <std::size_t... Is>
    struct Indices {
        using next = Indices<Is..., sizeof...(Is)>;
    };
    template <std::size_t N>
    struct IndicesBuilder {
        using type = typename IndicesBuilder<N-1>::type::next;
    };
    template <>
    struct IndicesBuilder<0> {
        using type = Indices<>;
    };
    template <std::size_t N>
    using BuildIndices = typename IndicesBuilder<N>::type;

    template <size_t N, typename R>
    struct MethodCaller
    {
        private:
            template <typename T, typename ... Args, size_t ... I>
            Variant call(T& obj, R (T::*ptr)(Args...), const std::vector<Variant> &args, Indices<I...>){
                return (obj.*ptr)(static_cast<Args>(args[I])...);
            }
            template <typename T, typename ... Args, size_t ... I>
            Variant call(const T& obj, R (T::*ptr)(Args...) const, const std::vector<Variant> &args, Indices<I...>){
                return (obj.*ptr)(static_cast<Args>(args[I])...);
            }

        public:
            template <typename T, typename ... Args>
            Variant call (T& obj, R (T::*ptr)(Args...), const std::vector<Variant> &args) {
                if(args.size() != N) {
                    throw std::runtime_error("invalid argument count");
                }
                return call(obj, ptr, args, BuildIndices<N>{});
            }
            template <typename T, typename ... Args>
            Variant call (const T& obj, R (T::*ptr)(Args...) const, const std::vector<Variant> &args) {
                if(args.size() != N) {
                    throw std::runtime_error("invalid argument count");
                }
                return call(obj, ptr, args, BuildIndices<N>{});
            }
    };
    template <size_t N>
    struct MethodCaller<N, void>
    {
        private:
            template <typename T, typename ... Args, size_t ... I>
            void call(T& obj, void (T::*ptr)(Args...), const std::vector<Variant> &args, Indices<I...>){
                (obj.*ptr)(static_cast<Args>(args[I])...);
            }
            template <typename T, typename ... Args, size_t ... I>
            void call(const T& obj, void (T::*ptr)(Args...) const, const std::vector<Variant> &args, Indices<I...>){
                (obj.*ptr)(static_cast<Args>(args[I])...);
            }

        public:
            template <typename T, typename ... Args>
            void call (T& obj, void (T::*ptr)(Args...), const std::vector<Variant> &args) {
                if(args.size() != N) {
                    throw std::runtime_error("invalid argument count");
                }
                call(obj, ptr, args, BuildIndices<N>{});
            }
            template <typename T, typename ... Args>
            void call (const T& obj, void (T::*ptr)(Args...) const, const std::vector<Variant> &args) {
                if(args.size() != N) {
                    throw std::runtime_error("invalid argument count");
                }
                call(obj, ptr, args, BuildIndices<N>{});
            }
    };
}


struct Class;
struct Method;

struct Type {
    virtual ~Type() {}
    virtual const std::string& name() const = 0;
    virtual Variant create() const = 0;
    virtual const Class* asClass() const { return nullptr; }
};

struct Property {
    virtual ~Property() {}
    virtual const std::string& name() const = 0;
    virtual const Type* type() const = 0;
    virtual void set(Variant&, const Variant&) const = 0;
    virtual Variant get(const Variant&) const = 0;
};

struct Method {
    virtual ~Method() {}
    virtual const std::string& name() const = 0;
    virtual const Type* returns() const = 0;
    virtual const std::vector<const Type*>& arguments() const = 0;
    virtual Variant call(Variant&, const std::vector<Variant>&) const = 0;
    virtual Variant call(const Variant&, const std::vector<Variant>&) const {
        throw std::runtime_error("invalid call");
    }
    template<typename ... Args>
    Variant call(Variant& obj, Args ... args) const {
        std::vector<Variant> vec = { std::forward<Args>(args)... };
        return call(obj, vec);
    }
    template<typename ... Args>
    Variant call(const Variant& obj, Args ... args) const {
        std::vector<Variant> vec = { std::forward<Args>(args)... };
        return call(obj, vec);
    }
};

struct Constructor {
    virtual ~Constructor() {}
    virtual const Type* creates() const = 0;
    virtual const std::vector<const Type*>& arguments() const = 0;
    virtual Variant call(const std::vector<Variant>&) const = 0;
    template<typename ... Args>
    Variant call(Args ... args) const {
        std::vector<Variant> vec = { std::forward<Args>(args)... };
        return call(vec);
    }
};

struct Class : Type {
    virtual ~Class() {}
    virtual const std::vector<const Constructor*>& constructors() const = 0;
    virtual const std::vector<const Method*>& methods() const = 0;
    virtual const std::vector<const Property*>& properties() const = 0;
    virtual const std::vector<const Type*>& bases() const = 0;
    virtual const Property* property(const std::string& name) const = 0;
    virtual const Method* method(const std::string& name) const = 0;
    virtual const Class* asClass() const { return this; }
};

template<typename T>
struct TypeImpl : Type {
    Variant create() const { return Variant(T()); }
    const std::string& name() const { return name_; }
    void name(std::string v) { name_ = std::move(v); }
    std::string name_;
};

template<>
struct TypeImpl<void> : Type {
    Variant create() const { return Variant(); }
    const std::string& name() const { return name_; }
    void name(std::string v) { name_ = std::move(v); }
    std::string name_;
};

template<typename T>
struct Dispatcher {};

template<typename T>
const Type* GetType(Dispatcher<T>) { return T::getType(); }

class TypeRegister {
        template<typename T>
        static const Type* createBasic(const char* name) {
            static_assert(std::is_same<typename std::decay<T>::type, T>::value, "invalid type");
            auto type = new TypeImpl<T>();
            type->name(name);
            return type;
        }
    public:
        template<typename T>
        static const Type* getBasic(const char* name) {
            static const Type* t = createBasic<T>(name);
            return t;
        }

    public:
        template<typename T>
        static void add(const Type* type) {
            typeById[TypeTraits<T>::uuid()] = type;
        }
        template<typename T>
        static const Type* get() {
            using U = typename std::decay<T>::type;
            if(typeById.find(TypeTraits<U>::uuid()) == typeById.end()) {
                const Type* type = GetType(Dispatcher<U>{});
                add<U>(type);
                return type;
            }
            return get(TypeTraits<U>::uuid());
        }
        static const Type* get(uint64_t id) {
            if(typeById.find(id) == typeById.end()) {
                throw std::runtime_error("type not found");
            }
            return typeById[id];
        }

    private:
        static std::map<uint64_t, const Type*> typeById;
};

std::map<uint64_t, const Type*> TypeRegister::typeById;

#define XSTR(x) #x
#define STR(x) XSTR(x)

#define DEFINE_GETTYPE(type) const Type* GetType(Dispatcher<type>) { return TypeRegister::getBasic<type>(STR(type)); }

DEFINE_GETTYPE(void)
DEFINE_GETTYPE(char)
DEFINE_GETTYPE(bool)
DEFINE_GETTYPE(int8_t)
DEFINE_GETTYPE(uint8_t)
DEFINE_GETTYPE(int16_t)
DEFINE_GETTYPE(uint16_t)
DEFINE_GETTYPE(int32_t)
DEFINE_GETTYPE(uint32_t)
DEFINE_GETTYPE(int64_t)
DEFINE_GETTYPE(uint64_t)
DEFINE_GETTYPE(float)
DEFINE_GETTYPE(double)
DEFINE_GETTYPE(std::string)

template<typename T, typename U>
struct PropertyImpl : Property {
    PropertyImpl(U T::* ptr) : ptr_(ptr), type_(TypeRegister::get<U>()), getter_(nullptr), setter_(nullptr) {}
    const std::string& name() const { return name_; }
    void name(std::string v) { name_ = std::move(v); }
    const Type* type() const { return type_; }
    const Method* getter() const { return getter_; }
    void getter(const Method* v) { delete getter_; getter_ = v; }
    const Method* setter() const { return setter_; }
    void setter(const Method* v) { delete setter_; setter_ = v; }
    void set(Variant& obj, const Variant& val) const {
        if(setter_) {
            std::vector<Variant> args = { val };
            setter_->call(obj, args);
        } else {
            static_cast<T&>(obj).*ptr_ = static_cast<const U&>(val);
        }
    }
    Variant get(const Variant& obj) const {
        if(getter_) {
            return getter_->call(obj, {});
        } else {
            return static_cast<const T&>(obj).*ptr_;
        }
    }
    U T::* ptr_;
    const Type* type_;
    const Method* getter_;
    const Method* setter_;
    std::string name_;
};

template<typename R, typename ... Args>
struct MethodBaseImpl : Method {
    MethodBaseImpl() : returns_(TypeRegister::get<R>()), args_({ TypeRegister::get<Args>()... }) {}
    const std::string& name() const { return name_; }
    void name(std::string v) { name_ = std::move(v); }
    const Type* returns() const { return returns_; };
    void returns(const Type* v) { returns_ = v; };
    const std::vector<const Type*>& arguments() const { return args_; }
    void arguments(std::vector<const Type*> v) { args_ = std::move(v); }
    const Type* returns_;
    std::vector<const Type*> args_;
    std::string name_;
};

template<typename R, typename T, typename ... Args>
struct MethodImpl : MethodBaseImpl<R, Args...> {
    MethodImpl(R (T::*ptr)(Args...)) : ptr_(ptr) {}
    Variant call(Variant& obj, const std::vector<Variant>& args) const {
        return MethodCaller<sizeof...(Args), R>().call(static_cast<T&>(obj), ptr_, args);
    }
    R (T::*ptr_)(Args...);
};

template<typename T, typename ... Args>
struct MethodImpl<void, T, Args...> : MethodBaseImpl<void, Args...> {
    MethodImpl(void (T::*ptr)(Args...)) : ptr_(ptr) {}
    Variant call(Variant& obj, const std::vector<Variant>& args) const {
        MethodCaller<sizeof...(Args), void>().call(static_cast<T&>(obj), ptr_, args);
        return Variant();
    }
    void (T::*ptr_)(Args...);
};

template<typename R, typename T, typename ... Args>
struct ConstMethodImpl : MethodBaseImpl<R, Args...> {
    ConstMethodImpl(R (T::*ptr)(Args...) const) : ptr_(ptr) {}
    Variant call(Variant& obj, const std::vector<Variant>& args) const {
        return MethodCaller<sizeof...(Args), R>().call(static_cast<T&>(obj), ptr_, args);
    }
    Variant call(const Variant& obj, const std::vector<Variant>& args) const {
        return MethodCaller<sizeof...(Args), R>().call(static_cast<T&>(obj), ptr_, args);
    }
    R (T::*ptr_)(Args...) const;
};

template<typename T, typename ... Args>
struct ConstMethodImpl<void, T, Args...> : MethodBaseImpl<void, Args...> {
    ConstMethodImpl(void (T::*ptr)(Args...) const) : ptr_(ptr) {}
    Variant call(Variant& obj, const std::vector<Variant>& args) const {
        MethodCaller<sizeof...(Args), void>().call(static_cast<T&>(obj), ptr_, args);
        return Variant();
    }
    Variant call(const Variant& obj, const std::vector<Variant>& args) const {
        MethodCaller<sizeof...(Args), void>().call(static_cast<const T&>(obj), ptr_, args);
        return Variant();
    }
    void (T::*ptr_)(Args...) const;
};

template<typename T, typename ... Args>
struct ConstructorBaseImpl : Constructor {
    ConstructorBaseImpl(const Type* creates, std::vector<const Type*> args) : creates_(creates), args_(std::move(args)) {}
    const Type* creates() const { return creates_; }
    const std::vector<const Type*>& arguments() const { return args_; };
    Variant call(const std::vector<Variant>& args) const {
        if(args.size() != sizeof...(Args)) {
            throw std::runtime_error("invalid argument count");
        }
        return call(args, BuildIndices<sizeof...(Args)>{});
    }
    template<size_t ... Is>
    Variant call(const std::vector<Variant> &args, Indices<Is...>) const {
        T t(static_cast<Args>(args[Is])...);
        return Variant(std::move(t));
    }
    const Type* creates_;
    std::vector<const Type*> args_;
};

template<typename T, typename ... Args>
struct ConstructorImpl : ConstructorBaseImpl<T, Args...> {
    ConstructorImpl(const Type* creates) : ConstructorBaseImpl<T, Args...>(creates, {TypeRegister::get<Args>()...}) {}
};

template<typename T>
struct ConstructorImpl<T, T> : ConstructorBaseImpl<T, T> {
    ConstructorImpl(const Type* creates) : ConstructorBaseImpl<T, T>(creates, {creates}) {}
};

template<typename T>
struct NameComparator {
    bool operator()(const T* lhs, const T* rhs) const {
        return lhs->name() < rhs->name();
    }
    bool operator()(const T* lhs, const std::string& rhs) const {
        return lhs->name() < rhs;
    }
    bool operator()(const std::string& lhs, const T* rhs) const {
        return lhs < rhs->name();
    }
};

struct ConstructorComparator {
    bool operator()(const Constructor* lhs, const Constructor* rhs) const {
        return lhs->arguments().size() < rhs->arguments().size();
    }
    bool operator()(const Constructor* lhs, size_t rhs) const {
        return lhs->arguments().size() < rhs;
    }
    bool operator()(size_t lhs, const Constructor* rhs) const {
        return lhs < rhs->arguments().size();
    }
};


template<typename T>
struct ClassImpl : Class {
    ClassImpl(std::string v) : name_(std::move(v)) {}
    Variant create() const {
        if(constructors_.empty() || !constructors_[0]->arguments().empty())
            throw std::runtime_error("no default c-tor");
        return constructors_[0]->call({});
    }
    const std::vector<const Constructor*>& constructors() const { return constructors_; }
    const std::vector<const Method*>& methods() const { return methods_; }
    const std::vector<const Property*>& properties() const { return properties_; }
    const std::vector<const Type*>& bases() const { return bases_; }
    const std::string& name() const { return name_; }
    const Property* property(const std::string& name) const {
        auto its = std::equal_range(properties_.begin(), properties_.end(), name, NameComparator<Property>());
        if(its.first != its.second) {
            return *its.first;
        }
        std::string err;
        err += "property \"";
        err += name;
        err += "\" not found";
        throw std::runtime_error(err.c_str());
    }
    const Method* method(const std::string& name) const {
        auto its = std::equal_range(methods_.begin(), methods_.end(), name, NameComparator<Method>());
        if(its.first != its.second) {
            return *its.first;
        }
        std::string err;
        err += "method \"";
        err += name;
        err += "\" not found";
        throw std::runtime_error(err.c_str());
    }
    const Type* base(const std::string& name) const {
        auto its = std::equal_range(bases_.begin(), bases_.end(), name, NameComparator<Type>());
        if(its.first != its.second) {
            return *its.first;
        }
        std::string err;
        err += "base class \"";
        err += name;
        err += "\" not found";
        throw std::runtime_error(err.c_str());
    }
    std::vector<const Constructor*> constructors_;
    std::vector<const Method*> methods_;
    std::vector<const Property*> properties_;
    std::vector<const Type*> bases_;
    std::string name_;
};

template<typename T>
struct ClassBuilder;

template<typename T, typename U>
struct PropertyBuilder {
    PropertyBuilder(ClassBuilder<T>& cls, PropertyImpl<T, U>& prp) : cls_(cls), prp_(prp) {}
    PropertyBuilder& name(std::string v) { prp_.name(std::move(v)); return *this; }
    PropertyBuilder& getter(U (T::*ptr)() const) { prp_.getter(new ConstMethodImpl<U, T>(ptr)); return *this; }
    PropertyBuilder& getter(U (T::*ptr)()) { prp_.getter(new MethodImpl<U, T>(ptr)); return *this; }
    PropertyBuilder& setter(void (T::*ptr)(U)) { prp_.setter(new MethodImpl<void, T, U>(ptr)); return *this; }
    ClassBuilder<T>& done() { return cls_; }
    ClassBuilder<T>& cls_;
    PropertyImpl<T, U>& prp_;
};

template<typename R, typename T, typename ... Args>
struct MethodBuilder {
    MethodBuilder(ClassBuilder<T>& cls, MethodImpl<R, T, Args...>& mtd) : cls_(cls), mtd_(mtd) {}
    MethodBuilder& name(std::string v) { mtd_.name(std::move(v)); return *this; }
    ClassBuilder<T>& done() { return cls_; }
    ClassBuilder<T>& cls_;
    MethodImpl<R, T, Args...>& mtd_;
};

template<typename R, typename T, typename ... Args>
struct ConstMethodBuilder {
    ConstMethodBuilder(ClassBuilder<T>& cls, ConstMethodImpl<R, T, Args...>& mtd) : cls_(cls), mtd_(mtd) {}
    ConstMethodBuilder& name(std::string v) { mtd_.name(std::move(v)); return *this; }
    ClassBuilder<T>& done() { return cls_; }
    ClassBuilder<T>& cls_;
    ConstMethodImpl<R, T, Args...>& mtd_;
};

template<typename T, typename ... Args>
struct ConstructorBuilder {
    ConstructorBuilder(ClassBuilder<T>& cls, ConstructorImpl<T, Args...>& ctor) : cls_(cls), ctor_(ctor) {}
    ClassBuilder<T>& done() { return cls_; }
    ClassBuilder<T>& cls_;
    ConstructorImpl<T, Args...> ctor_;
};

template<typename T>
struct BaseBuilder {
    BaseBuilder(ClassBuilder<T>& cls) : cls_(cls) {}
    ClassBuilder<T>& done() { return cls_; }
    ClassBuilder<T>& cls_;
};

template<typename T>
struct ClassBuilder {
    ClassBuilder(std::string name) : cls_(new ClassImpl<T>(std::move(name))) {}
    ~ClassBuilder() { delete cls_; }
    template<typename R, typename ... Args>
    MethodBuilder<R, T, Args...> add(R (T::*ptr)(Args...)) {
        auto mtd = new MethodImpl<R, T, Args...>(ptr);
        cls_->methods_.push_back(mtd);
        return MethodBuilder(*this, *mtd);
    }
    template<typename R, typename ... Args>
    ConstMethodBuilder<R, T, Args...> add(R (T::*ptr)(Args...) const) {
        auto mtd = new ConstMethodImpl<R, T, Args...>(ptr);
        cls_->methods_.push_back(mtd);
        return ConstMethodBuilder(*this, *mtd);
    }
    template<typename U>
    PropertyBuilder<T, U> add(U T::*ptr) {
        auto prp = new PropertyImpl<T, U>(ptr);
        cls_->properties_.push_back(prp);
        return PropertyBuilder(*this, *prp);
    }
    template<typename ... Args>
    ConstructorBuilder<T, Args...> ctor() {
        auto ctor = new ConstructorImpl<T, Args...>(cls_);
        cls_->constructors_.push_back(ctor);
        return ConstructorBuilder(*this, *ctor);
    }
    template<typename U>
    BaseBuilder<T> base() {
        auto type = TypeRegister::get<U>();
        cls_->bases_.push_back(type);
        return BaseBuilder(*this);
    }
    const Type* result() {
        std::sort(cls_->properties_.begin(), cls_->properties_.end(), NameComparator<Property>());
        std::sort(cls_->methods_.begin(), cls_->methods_.end(), NameComparator<Method>());
        std::sort(cls_->constructors_.begin(), cls_->constructors_.end(), ConstructorComparator());
        std::sort(cls_->bases_.begin(), cls_->bases_.end(), NameComparator<Type>());
        const Type* cls = cls_;
        cls_ = nullptr;
        return cls;
    }
    ClassImpl<T>* cls_;
};

struct TestBase {
   static const Type* getType() {
        static const Type* type = ClassBuilder<TestBase>("TestBase")
            .ctor<>()
                .done()
            .ctor<TestBase>()
                .done()
            .result();
        return type;
    }
};

struct Test : TestBase {
    static const Type* getType() {
        static const Type* type = ClassBuilder<Test>("Test")
            .base<TestBase>()
                .done()
            .ctor<>()
                .done()
            .ctor<Test>()
                .done()
            .add(&Test::property)
                .name("prop")
                .done()
            .add(&Test::method)
                .name("method")
                .done()
            .add(&Test::method2)
                .name("method2")
                .done()
            .result();
        return type;
    }
    Test() : property(123) {}
    int property;
    int method(int v) { return property + v; }
    void method2(std::string v) const { std::cout << v << std::endl; }
};

void print(const Class* t) {
    std::cout << "type: " << t->name() << std::endl;

    for(auto b : t->bases()) {
        std::cout << "base: " << std::endl;
        std::cout << "=======" << std::endl;
        print(b->asClass());
        std::cout << "=======" << std::endl;
    }

    for(auto c : t->constructors()) {
        std::cout << "ctor: " << c->creates()->name() << "(";
        for(auto a : c->arguments()) {
            std::cout << a->name();
        }
        std::cout << ")" << std::endl;
    }

    for(auto m : t->methods()) {
        std::cout << "method: " << m->returns()->name() << " " << m->name() << "(";
        for(auto a : m->arguments()) {
            std::cout << a->name();
        }
        std::cout << ")" << std::endl;
    }

    for(auto p : t->properties()) {
        std::cout << "property: " << p->type()->name() << " " << p->name() << std::endl;
    }
}

int main(int argc, char** argv)
{

    auto t = TypeRegister::get<Test>()->asClass();
    std::cout << "stage 0" << std::endl;
    auto v = t->create();
    std::cout << "stage 1" << std::endl;
    auto m = t->method("method");
    std::cout << "stage 2" << std::endl;
    auto r = m->call(v, 2);
    std::cout << "stage 3" << std::endl;
    std::cout << static_cast<int>(r) << std::endl;
    std::cout << "stage 4" << std::endl;
    t->method("method2")->call(v, "message");
    std::cout << "stage 5" << std::endl;

    print(t);

    auto v2 = t->constructors()[1]->call({v});
    t->method("method2")->call(v2, "message 2");

    return 0;
}
