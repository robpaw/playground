#pragma once

#include <tuple>

#include <cstddef>

template<typename T>
inline T & stay(T && t) {
  return t;
}

struct Expand {
  template <typename... T>
  inline Expand(T&&...) {}
};

template<typename T, typename U>
inline static U Identity(U u) { 
  return u;
};

template<size_t ... Is>
struct Indices {};
template<size_t N, size_t V, size_t ... Is>
struct BuildIndices : BuildIndices<N-1, V-1, V, Is...> {}; 
template<size_t V, size_t ... Is>
struct BuildIndices<1, V, Is...> : Indices<V, Is...> {};
template<size_t V, size_t ... Is>
struct BuildIndices<0, V, Is...> : Indices<> {};
template <typename Tuple>
using IndicesFor = BuildIndices<std::tuple_size<Tuple>::value, std::tuple_size<Tuple>::value - 1>;

template <size_t From, size_t To>
struct Slice : BuildIndices<To - From + 1, To> {
  static_assert(To >= From, "invalid range");
}; 

template<size_t I, typename Tuple>
using ToType = typename std::tuple_element<I, Tuple>::type;

template <typename T, typename Tuple>
struct ToIndex;
template <typename T, typename ... Ts>
struct ToIndex<T, std::tuple<T, Ts ...>> {
  static const std::size_t value = 0;
};
template <typename T, typename U, typename ... Ts>
struct ToIndex<T, std::tuple<U, Ts ...>> {
  static const std::size_t value = 1 + ToIndex<T, std::tuple<Ts...>>::value;
};
