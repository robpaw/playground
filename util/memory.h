#pragma once

#include <stdexcept>

#include <cstddef>

void * align(size_t alignment, size_t size, void*& ptr, size_t space) {
  using byte_t = char;
  byte_t* p = (byte_t*)(((uintptr_t)ptr + (uintptr_t)(alignment - 1)) & ~((uintptr_t)(alignment - 1)));
  size_t offset = p - (byte_t*)ptr;
  if(size + offset <= space) {
    space -= size + offset;
    ptr = (void*)(p + size);
    return p;
  }
  throw std::runtime_error("not enough space");
}
